﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Deployment;
using System.Reflection;
using System.Deployment.Application;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySqlDataManager;
using TechnoSuiteRobot;
namespace FACMTechnoAutomatic
{
    public partial class MainMenu : Form
    {
        const short FEATURE_SIZE = 100;
        public enum Feature
        {
            SAVE_SETTING=0, HISTORY, EXPORT_HISTORY
        };

        private bool[] feature = new bool[FEATURE_SIZE];
        private IFACMObjectClass application;
        private bool LoadFirst = true;

        internal static bool admin = false;
        internal static MySqlDataLayer dataLayer = new MySqlDataLayer(FACMTechnoAutomatic.Properties.Settings.Default.ConnectionString);

        internal void FocusThis()
        {
            this.Activate();
        }

        public MainMenu()
        {
            InitializeComponent();

            this.KeyPress += new KeyPressEventHandler(MainMenu_KeyPress);
            this.navBarItemParameters.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(navBarItemParameters_LinkClicked);
            this.navBarItemPassword.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(navBarItemPassword_LinkClicked);
            this.navBarControlMain.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(navBarControlMain_LinkClicked);

            application = GetApplication(FACMTechnoAutomatic.Properties.Settings.Default.Project);
            PreloadForm();
            LoadSetting();
            LoadFeature();
        }

        void navBarControlMain_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            LoadForm(this.application.GetInterfaceForm());
        }

        
       #region Link Event
        void navBarItemParameters_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (MainMenu.admin)
                LoadForm(new FormParameters(this, application));
            else
                MessageBox.Show("You Must have Administrator Previledge", "Unrestricted Access", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        void navBarItemPassword_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            new PasswordForm().ShowDialog(this);
        }

        
        #endregion

        private void MainMenu_Load(object sender, EventArgs e)
        {
            // version
            labelControlVersion.Text = "Version " + this.GetRunningVersion();

            if (LoadFirst)
            {
                Thread.Sleep(5000);

                int delay = FACMTechnoAutomatic.Properties.Settings.Default.Delay;
                TechnoRobot robot = new TechnoRobot(delay);
                robot.FocusTechnoSuite();
                Thread.Sleep(500);
                bool suc = false;
                string query = null;
                while (!suc)
                {
                    try
                    {
                        query = MainMenu.dataLayer.SelectQuery(
                            "select TWMName from FACM_Active where Project = '" +
                            application.GetProjectName() + "'"
                            ).Rows[0]["TWMName"].ToString();
                        suc = true;
                    }
                    catch (Exception) { }
                }
                robot.LoadFile(query);
                Thread.Sleep(500);
                robot.SetConnection();
            }
            LoadForm(application.GetInterfaceForm());
            this.Activate();
        }

        private void LoadForm(Form child)
        {
            if (this.ActiveMdiChild != null)
                if (Object.ReferenceEquals(this.ActiveMdiChild.GetType(), child.GetType()))
                    return;
            if (this.ActiveMdiChild != null)
                this.ActiveMdiChild.Dispose();
            
            this.ActivateMdiChild(child);
            child.MdiParent = this;
            child.Activate();
            child.Show();
        }

        internal void ChangeProject(string project)
        {
            application = GetApplication(project);
            PreloadForm();
            LoadSetting();
        }

        private void LoadSetting()
        {
            DataTable dat = MainMenu.dataLayer.SelectQuery(
                "select * from FACM_Active where project = '" +
                application.GetProjectName() + "'"
            );

            FACMTechnoAutomatic.Properties.Settings.Default.LogoSequence = dat.Rows[0]["logosequence"].ToString();
            FACMTechnoAutomatic.Properties.Settings.Default.MixedDefault = dat.Rows[0]["mixeddefault"].ToString();
            FACMTechnoAutomatic.Properties.Settings.Default.PrintingTime = int.Parse(dat.Rows[0]["printingtime"].ToString());
            FACMTechnoAutomatic.Properties.Settings.Default.ExternalPath = dat.Rows[0]["externalpath"].ToString().Replace("#", "\\");
            FACMTechnoAutomatic.Properties.Settings.Default.Delay = int.Parse(dat.Rows[0]["delay"].ToString());
            FACMTechnoAutomatic.Properties.Settings.Default.Password = bool.Parse(dat.Rows[0]["password"].ToString());
            FACMTechnoAutomatic.Properties.Settings.Default.DefaultFont = bool.Parse(dat.Rows[0]["defaultfont"].ToString());
            FACMTechnoAutomatic.Properties.Settings.Default.Save();
        }

        internal bool IsFeatureOn(Feature feat)
        {
            return feature[(int)feat];
        }

        private void PreloadForm()
        {
            this.labelControlFor.Text = "Designed For: " + application.GetProjectName();
            UnLoadForm();
        }

        void MainMenu_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '`')
            {
                new ChangeProjectForm(this).ShowDialog(this);
            }
        }

        private void LoadFeature()
        {
            feature[(int)Feature.SAVE_SETTING] = false;
            feature[(int)Feature.HISTORY] = true;
            feature[(int)Feature.EXPORT_HISTORY] = true;
        }
   
        private IFACMObjectClass GetApplication(string project)
        {
            IFACMObjectClass application = null;
            switch (project)
            {
                case "Hino Small Plate":
                    application = new HinoSmallPlate(this);
                    break;
                case "Hino Machine Block":
                    application = new HinoMachineBlock(this);
                    break;
                case "Hino Medium Chasis":
                    application = new HinoMediumChasis(this);
                    break;
                case "Hino Small Chasis":
                    application = new HinoSmallChasis(this);
                    break;
                case "Hino Medium Plate":
                    application = new HinoMediumPlate(this);
                    break;
                case "Cartepillar":
                    application = new Cartepillar(this);
                    break;
            }
            return application;
        }

        internal IFACMObjectClass GetProject()
        {
            return application;
        }

        private void UnLoadForm()
        {
            if (this.ActiveMdiChild != null)
                this.ActiveMdiChild.Dispose();
        }

        private void radioGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radioGroup.SelectedIndex == 0)
            {
                new PasswordRequest().ShowDialog(this);

                if (!MainMenu.admin)
                    radioGroup.SelectedIndex = 1;
            }
            else
            {
                MainMenu.admin = false;
            }
            UnLoadForm();
        }

        private void navBarItemExit_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            Dispose();
            Application.Exit();
        }

        private void navBarItemHistory_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (feature[(int)Feature.HISTORY])
            {
                if (admin)
                    LoadForm(new History(this));
                else
                    MessageBox.Show("You must have an admin previledge to do this.","Not Enough Previledge", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Feature is disabled.", "Cannot Open Feature", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void labelControlVersion_Click(object sender, EventArgs e)
        {

        }

        private Version GetRunningVersion()
        {
            try
            {
                return System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion;
            }
            catch
            {
                return Assembly.GetExecutingAssembly().GetName().Version;
            }
        }
    }
}