﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FACMTechnoAutomatic
{
    public partial class ChangeProjectForm : Form
    {
        private MainMenu parent;
        
        public ChangeProjectForm(MainMenu parent)
        {
            this.parent = parent;
            InitializeComponent();
        }

        private void ChangeProjectForm_Load(object sender, EventArgs e)
        {
            // Load Project List
            DataTable dat = PopulateProject();
            this.lookUpEditProject.Properties.DataSource = dat;
            this.lookUpEditProject.Properties.DisplayMember = "Project";
        }

        private DataTable PopulateProject()
        {
            DataTable ret = MainMenu.dataLayer.SelectQuery(
                "select project as Project from facm_active where active"
            );

            return ret;
        }

        private void simpleButtonChange_Click(object sender, EventArgs e)
        {
            if (this.lookUpEditProject.EditValue == null)
            {
                MessageBox.Show("Please select at least 1 project.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string password = (string)MainMenu.dataLayer.ExecuteScalarQuery(
                "select password from FACM_Account where username = 'Super Administrator'"
            );

            string password_admin = (string)MainMenu.dataLayer.ExecuteScalarQuery(
                "select password from FACM_Account where username = 'Administrator'"
            );

            if (password == this.textEditPassword.Text || password_admin == this.textEditPassword.Text)
            {
                FACMTechnoAutomatic.Properties.Settings.Default.Project =
                    (this.lookUpEditProject.EditValue as DataRowView)[0].ToString();
                FACMTechnoAutomatic.Properties.Settings.Default.Save();

                parent.ChangeProject(FACMTechnoAutomatic.Properties.Settings.Default.Project);
                MessageBox.Show("Program Updated. ", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Wrong Password", "Wrong Password", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Dispose();
        }
    }
}
