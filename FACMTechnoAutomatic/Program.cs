﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Data;
using System.Management;

namespace FACMTechnoAutomatic
{
    static class Program
    {

        //static DataManager.DataLayer dat = new DataManager.DataLayer(FACMTechnoAutomatic.Properties.Settings.Default.ConnectionString);
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainMenu());
            
        }
    }
}
