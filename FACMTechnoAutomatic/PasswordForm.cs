﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace FACMTechnoAutomatic
{
    public partial class PasswordForm : DevExpress.XtraEditors.XtraForm
    {
        public PasswordForm()
        {
            InitializeComponent();

            LoadUsername(lookUpEditTarget);
            LoadUsername(lookUpEditUserName);
        }

        private void PasswordForm_Load(object sender, EventArgs e)
        {
               


        }

        private void LoadUsername(LookUpEdit target)
        {
            DataTable dat = MainMenu.dataLayer.SelectQuery(
                "select username as Username from FACM_Account"
            );

            target.Properties.DataSource = dat;
            target.Properties.DisplayMember = "Username";

        }

        private void simpleButtonCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void simpleButtonChange_Click(object sender, EventArgs e)
        {
            if (lookUpEditUserName.EditValue == null)
            {
                MessageBox.Show("Please Select the UserName.", "UserName Not Selected",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (lookUpEditTarget.EditValue == null)
            {
                MessageBox.Show("Please Select the target.", "Target Not Selected",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            string userName = (this.lookUpEditUserName.EditValue as DataRowView).Row["username"].ToString();
            string targetName = (this.lookUpEditTarget.EditValue as DataRowView).Row["Username"].ToString();

            DataTable dat = MainMenu.dataLayer.SelectQuery("select password from FACM_Account where " + 
                "username = '" + userName + "'");

            string realPassword = dat.Rows[0]["password"].ToString();

            if (realPassword != textEditPassword.Text)
            {
                MessageBox.Show("Wrong Password", "Wrong Password",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.ResetField();
                textEditPassword.Focus();
                return;
            }

            if (textEditNewPassword.Text != textEditConfirm.Text)
            {
                MessageBox.Show("New Password does not match.", "Not Match",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.ResetField();

                textEditNewPassword.Focus();
                return;
            }

            int userLevel = int.Parse(MainMenu.dataLayer.ExecuteScalarQuery(
                "select UserLevel from FACM_Account where username = '" +
                userName + "'"
            ));

            int targetLevel = int.Parse(MainMenu.dataLayer.ExecuteScalarQuery(
                "select UserLevel from FACM_Account where username = '" +
                targetName + "'"
            ));

            if (userLevel < targetLevel)
            {
                MessageBox.Show("You don't Have enough Previledge to do that.", "Not Enough Previledge",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

                this.ResetField();
                return;
            }


            MainMenu.dataLayer.ExecuteQuery("update FACM_Account set password = '" + this.textEditNewPassword.Text +
                "' where username = '" + targetName + "'");
            
            MessageBox.Show("Password is changed successfully.", "Notification", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
            this.Dispose();
        }

        private void ResetField()
        {
            this.textEditNewPassword.Text = "";
            this.textEditConfirm.Text = "";
            this.textEditPassword.Text = "";
        }
    }
}