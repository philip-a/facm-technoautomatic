﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TechnoSuiteRobot;

namespace FACMTechnoAutomatic
{
    class ApplicationManager
    {
        public static void ShutDownComputer(IFACMObjectClass obj, Form form)
        {
            TechnoRobot robot = new TechnoRobot(FACMTechnoAutomatic.Properties.Settings.Default.Delay);
            robot.SaveAndClose(obj.GetInputNumbers());
            System.Diagnostics.Process.Start("shutdown", "/s /f /t 1 /c " + (char)34 + "YOUR COMPUTER WILL BE TURNED OFF IN 1 seconds" + (char)34);
            form.Focus();
            form.Activate();
        }
    }
}
