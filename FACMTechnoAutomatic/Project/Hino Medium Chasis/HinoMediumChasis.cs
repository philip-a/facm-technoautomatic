﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TechnoSuiteRobot;

namespace FACMTechnoAutomatic
{
    class HinoMediumChasis : IFACMObjectClass
    {
        private Form form;
        private MainMenu parent;
        internal HinoMediumChasis(MainMenu parent)
        {
            this.parent = parent;
        }
        
        public string GetProjectName()
        {
            return "Hino Medium Chasis";
        }

        public Form GetInterfaceForm()
        {
            form = new HinoMediumChasisForm(parent);
            return form;
        }

        public int GetInputNumbers()
        {
            return 2;
        }

        public string GetDifferenceName()
        {
            return "HinoMediumChasisDifference";
        }

        public string GetPrimarySetName()
        {
            return "HinoMediumChasisPrimarySet";
        }

        public string GetParameterName()
        {
            return "HinoMediumChasisParameter";
        }
    }
}
