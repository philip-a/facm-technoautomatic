﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TechnoSuiteRobot;

namespace FACMTechnoAutomatic
{
    class Cartepillar : IFACMObjectClass
    {
        private Form form;
        private MainMenu parent;
        internal Cartepillar(MainMenu parent)
        {
            this.parent = parent;
        }
        
        public string GetProjectName()
        {
            return "Cartepillar";
        }

        public Form GetInterfaceForm()
        {
            form = new CartepillarForm(parent);
            return form;
        }

        public int GetInputNumbers()
        {
            return 1;
        }

        public string GetDifferenceName()
        {
            return "CartepillarDifference";
        }

        public string GetPrimarySetName()
        {
            return "CartepillarPrimarySet";
        }

        public string GetParameterName()
        {
            return "CartepillarParameter";
        }
    }
}
