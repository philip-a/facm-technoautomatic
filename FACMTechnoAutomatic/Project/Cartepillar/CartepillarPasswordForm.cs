﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FACMTechnoAutomatic
{
    public partial class CartepillarPasswordForm : Form
    {

        private CartepillarForm parent;
        public CartepillarPasswordForm(CartepillarForm parent)
        {
            InitializeComponent();
            this.parent = parent;
        }


        private void InputPasswordForm_Load(object sender, EventArgs e)
        {

        }

        private void simpleButtonOK_Click(object sender, EventArgs e)
        {
            DataTable dat = MainMenu.dataLayer.SelectQuery(
                "select password from FACM_Account"
            );

            string password = this.textEditPassword.Text;

            foreach (DataRow x in dat.Rows)
                if (x["password"].ToString() == password)
                {
                    parent.SetPasswordBoolean(true);
                    Dispose();
                    return;
                }

            MessageBox.Show("Wrong Password", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            parent.SetPasswordBoolean(false);
            Dispose();
        }
    }
}
