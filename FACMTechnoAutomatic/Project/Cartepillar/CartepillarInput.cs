﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FACMTechnoAutomatic
{
    public partial class CartepillarInput : Form
    {
        private CartepillarForm parent;
        public CartepillarInput(CartepillarForm parent)
        {
            this.parent = parent;
            InitializeComponent();
        }

        private void simpleButtonOK_Click(object sender, EventArgs e)
        {
            parent.SetCartepillarLine(textEditInput.Text);
            Dispose();
        }


    }
}
