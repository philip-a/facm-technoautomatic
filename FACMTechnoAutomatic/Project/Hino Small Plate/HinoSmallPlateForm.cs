﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Threading;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SendKeys;
using TechnoSuiteRobot;
using DevExpress.XtraEditors;

namespace FACMTechnoAutomatic
{
    public partial class HinoSmallPlateForm : Form
    {
       #region Class Flag 
        private bool Loaded = false;
        private bool readyToPrint = false;
        private bool passwordTrue = false;
        private bool confirmPrint = false;
        private bool wantToStop = false;
        private bool shutDown = false;
        private bool isPrinting = false;
        private Thread WaitTillFinish;
        private delegate void SetTextCallback(TextEdit edit, string text);
        private delegate void SetStatusColorCallBack(Color color, string text);
        #endregion
       
        #region Fields
        private int sequence = 0;
        private Hashtable database = new Hashtable();
        private MainMenu parent;
        #endregion

        #region Resource Preload
        public HinoSmallPlateForm(MainMenu parent)
        {
            InitializeComponent();
            this.parent = parent;
        }

        private void HinoSmallPlateForm_Load(object sender, EventArgs e)
        {
            Console.WriteLine("Main Form Loaded, begin Garbage Collecting");
            dateEditTime.DateTime = DateTime.Now;
            GC.Collect();
        }
        #endregion
        
        #region Main Listener
        private void HinoSmallPlateForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.NumPad7 || e.KeyCode == Keys.D7)
            {
                if (!isPrinting)
                    return;

                Console.WriteLine("Attempting to stop the Program.");
                new ConfimationProceedForm(this, new Thread(new ThreadStart(SetStop)), "Want to Stop?").ShowDialog(this);

                if (wantToStop)
                {
                    int delay = FACMTechnoAutomatic.Properties.Settings.Default.Delay;
                    Console.WriteLine("Confirmed by user, preparing to stop TechnoMark.");
                    TechnoRobot stoper = new TechnoRobot(delay);
                    stoper.FocusTechnoSuite();
                    Thread.Sleep(200);
                    stoper.TechnoStop();
                    try { WaitTillFinish.Abort(); }
                    catch { }
                    Console.WriteLine("Finished sending command to stop");
                    Console.WriteLine("Begin to clear all fields and reactivate program");
                    isPrinting = false;
                    SetStatusColor(Color.Lime, "WAITING");
                    ResetField();
                    parent.FocusThis();
                    wantToStop = false;
                    Console.WriteLine("All set, Jobs are finished.");
                }
                else
                {
                    Console.WriteLine("Command is aborted.");
                }
                return;
            }

            if (isPrinting)
                return;

            if (e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.D1)
            {

                String path = FACMTechnoAutomatic.Properties.Settings.Default.ExternalPath;
                path += "WOS-" + ValidateDate(dateEditTime.DateTime.Day.ToString()) +
                    ValidateDate(dateEditTime.DateTime.Month.ToString()) +
                    dateEditTime.DateTime.Year + ".txt";
                
                String[] messages = null;
                try
                {
                    messages = System.IO.File.ReadAllLines(@path);
                }
                catch (Exception x)
                {
                    x.ToString();
                    MessageBox.Show("Could not found WOS, Try input another date or specify the Path. Try to load from=" + path, "Error", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }

                foreach (string message in messages)
                {
                    int seq = int.Parse(message.Substring(0, 3));
                    if (!database.Contains(seq))
                        database.Add(seq, new Plate(message));
                    else
                        MessageBox.Show("Duplicate Sequence: " + seq + " on WOS, Last Sequence is Ignored", "Duplicate Sequence",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                MessageBox.Show("WOS loaded", "Finished loading", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Loaded = true;

                GC.Collect();
            }
            else if (e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.D2)
            {
                if (!Loaded)
                {
                    MessageBox.Show("WOS.txt is not Loaded", "Not Loaded", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }

                new HinoSmallPlateInput(this).ShowDialog(this);

                if (sequence == 0)
                    return;

                Plate loadedPlated = database[sequence] as Plate;

                if (loadedPlated != null)
                {
                    LoadSequence(sequence, loadedPlated);
                    readyToPrint = true;
                }
                else
                {
                    MessageBox.Show("Could not found Sequence " + sequence + " in current WOS", "Not Found",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    readyToPrint = false;
                }
                GC.Collect();
            } 
            else if (e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.D3)
            {
                new DateChangeForm(this).ShowDialog(this);
            }
            else if (e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.D4 || e.KeyCode == Keys.NumPad5 || e.KeyCode == Keys.D5)
            {
                bool isStart = false;
                if (e.KeyCode == Keys.D4 || e.KeyCode == Keys.NumPad4)
                    isStart = true;

                if (readyToPrint)
                {
                    DataTable dat = MainMenu.dataLayer.SelectQuery(
                        "select * from FACM_History where Date = '" +
                         EditDate(this.dateEditTime.DateTime) + "' and " +
                        "Sequence = " + sequence.ToString()
                    );

                    IFACMObjectClass obj = parent.GetProject();

                    bool isPrinted = dat.Rows.Count != 0;
                    bool passwordEnable = FACMTechnoAutomatic.Properties.Settings.Default.Password;

                    if (isPrinted && passwordEnable && isStart)
                    {
                        new InputPasswordForm(this).ShowDialog(this);
                        if (!passwordTrue)
                            return;
                    }
                    passwordTrue = false;

                    new ConfimationProceedForm(this, new Thread(new ThreadStart(SetConfirmationPrint)), "Ready to Print?").ShowDialog(this);

                    // executing phase
                    if (confirmPrint)
                    {
                        int delay = FACMTechnoAutomatic.Properties.Settings.Default.Delay;
                        TechnoSuiteRobot.TechnoRobot robot = new TechnoSuiteRobot.TechnoRobot(
                            FACMTechnoAutomatic.Properties.Settings.Default.LogoSequence,
                            FACMTechnoAutomatic.Properties.Settings.Default.MixedDefault,
                            delay
                        );

                        string[] messages = {
                          textEditModel.Text,
                          textEditEngine.Text,
                          textEditFrame.Text,
                          textEditCapacity.Text,
                          textEditTrim.Text,
                          textEditColor.Text,
                          textEditTrans.Text,
                          textEditPlant.Text
                        };

                        TechnoInput[] inps = new TechnoInput[obj.GetInputNumbers()];
                        DataTable prms = MainMenu.dataLayer.SelectQuery(
                            "select * from facm_" + obj.GetParameterName() + " order by Ordering"
                        );


                        string offSet = MainMenu.dataLayer.SelectQuery(
                            "select BrandName from facm_" + obj.GetPrimarySetName() + " where PrimarySet = 1"
                        ).Rows[0]["BrandName"].ToString();

                        string brand = textEditBrand.Text;

                        for (int i = 0; i < obj.GetInputNumbers(); i++)
                        {
                            DataRow r = prms.Rows[i];

                            float X = float.Parse(r["X"].ToString());
                            float Y = float.Parse(r["Y"].ToString());
                            if (brand != offSet)
                            {
                                DataTable dataC = MainMenu.dataLayer.SelectQuery(
                                    "select X, Y from FACM_Difference where BrandName = '" +
                                    brand + "' and LineName = '" + r["Name"] + "'"
                                );
                                X += float.Parse(dataC.Rows[0]["X"].ToString());
                                Y += float.Parse(dataC.Rows[0]["Y"].ToString());
                            }

                            if (FACMTechnoAutomatic.Properties.Settings.Default.DefaultFont)
                                robot.StandardFont = true;

                            inps[i] = new TechnoInput(messages[i],
                                new TechnoParam(X.ToString(), true),
                                new TechnoParam(Y.ToString(), true),
                                new TechnoParam(r["Height"].ToString(), true),
                                new TechnoParam(r["Font"].ToString(), true),
                                new TechnoParam(r["Depth"].ToString(), true),
                                new TechnoParam(r["Angle"].ToString(), true),
                                new TechnoParam(r["Radius"].ToString(), true),
                                new TechnoParam(r["Compression"].ToString(), true),
                                new TechnoParam(r["Spacing"].ToString(), true),
                                new TechnoParam(r["Italic"].ToString(), true)
                            );
                        }

                        if (isStart)
                        {
                            Console.WriteLine("Executing Robot total input = " + inps.Length);
                            robot.ExecuteInput(inps, TechnoRobot.TechnoExecMode.START);
                            MainMenu.dataLayer.ExecuteQuery("insert into FACM_History values ('" +
                                EditDate(this.dateEditTime.DateTime) + "'," + sequence.ToString() + ",'" +
                                EditDateTime(DateTime.Now) + "')"
                            );
                        }
                        else
                            robot.ExecuteInput(inps, TechnoRobot.TechnoExecMode.TEST);


                        WaitTillFinish = new Thread(new ThreadStart(PrintListener));
                        WaitTillFinish.Start();

                        parent.FocusThis();
                    }
                    GC.Collect();
                    confirmPrint = false;
                    
                }
                else
                    MessageBox.Show("Sequence Not Loaded", "Not Loaded", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                isStart = false;

            }
            else if (e.KeyCode == Keys.D8 || e.KeyCode == Keys.NumPad8)
            {
                new ConfimationProceedForm(this, new Thread(new ThreadStart(SetShutDown)), "Want to ShutDown?").ShowDialog(this);

                if (shutDown)
                    ApplicationManager.ShutDownComputer(parent.GetProject(), parent);
            }
        }

        #endregion

        #region Internal Interface Layer
        internal void SetSequence(int seq)
        {
            this.sequence = seq;
        }

        internal void SetPasswordBoolean(bool pass)
        {
            this.passwordTrue = pass;
        }

        internal void SetConfirmationPrint()
        {
            this.confirmPrint = true;
        }

        internal void SetShutDown()
        {
            this.shutDown = true;
        }

        internal void SetStop()
        {
            this.wantToStop = true;
        }

        internal void ResetMapping()
        {
            Loaded = false;
            this.database.Clear();
        }
        #endregion

        #region Date Change
        internal void IncrementDate()
        {
            dateEditTime.DateTime = dateEditTime.DateTime.AddDays(1);
            ResetField();
            Loaded = false;
        }

        internal void DecrementDate()
        {
            dateEditTime.DateTime = dateEditTime.DateTime.AddDays(-1);
            ResetField();
            Loaded = false;
        }

        internal string EditDate(DateTime input)
        {
            int tanggal = input.Day;
            int bulan = input.Month;
            int tahun = input.Year;

            string date = (tanggal < 10 ? "0" : "") + tanggal.ToString();
            string month = (bulan < 10 ? "0" : "") + bulan.ToString();
            string year = tahun.ToString();

            return year + "/" + month + "/" + date;
        }

        internal string EditDateTime(DateTime input)
        {
            int hour = input.Hour;
            int minute = input.Minute;
            int second = input.Second;

            string hours = (hour < 10 ? "0" : "") + hour.ToString();
            string minutes = (minute < 10 ? "0" : "") + minute.ToString();
            string seconds = (second < 10 ? "0" : "") + second.ToString();

            return EditDate(input) + " " + hours + ":" + minutes + ":" + seconds;
        }

        private string ValidateDate(string inp)
        {
            if (inp.Length == 1)
                return "0" + inp;
            else
                return inp;
        }
        #endregion

        #region Local Routine
        private void PrintListener()
        {
            Console.WriteLine("Changing mode to printing");
            isPrinting = true;
            Console.WriteLine("Printing = " + isPrinting);
            SetStatusColor(Color.Red, "PRINTING");

            int sleep = FACMTechnoAutomatic.Properties.Settings.Default.PrintingTime;
            Console.WriteLine("Timer started, time = " + sleep + " miliseconds");
            Thread.Sleep(sleep);

            Console.WriteLine("Printing is done.");
            Console.WriteLine("Reseting all fields");
            ResetField();

            Console.WriteLine("Changing mode to waiting");
            isPrinting = false;
            SetStatusColor(Color.Lime, "WAITING");
            Console.WriteLine("Printing = "+ isPrinting);
            Console.WriteLine("Jobs done.");
        }

        private void SetText(TextEdit edit, string text)
        {
            if (edit.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { edit, text });
            }
            else
            {
                edit.Text = text;
            }
        }

        private void SetStatusColor(Color color, string text)
        {
            if (labelControlStatus.InvokeRequired)
            {
                SetStatusColorCallBack back = new SetStatusColorCallBack(SetStatusColor);
                this.Invoke(back, new object[] { color, text });
            }
            else
            {
                labelControlStatus.Text = text;
                labelControlStatus.ForeColor = color;
            }
        }

        private void ResetField()
        {
            SetText(textEditBrand, "");
            SetText(textEditCapacity, "");
            SetText(textEditColor, "");
            SetText(textEditEngine, "");
            SetText(textEditFrame, "");
            SetText(textEditModel, "");
            SetText(textEditPlant, "");
            SetText(textEditSequence, "");
            SetText(textEditTrans, "");
            SetText(textEditTrim, "");
            readyToPrint = false;
        }

        private string ValidateColon(string input)
        {
            string ret = "";
            foreach (char c in input)
                if (c == ',')
                    ret += ".";
                else
                    ret += c;
            return ret;
        }

        private void LoadSequence(int sequence, Plate input)
        {
            this.textEditSequence.Text = sequence.ToString();
            this.textEditBrand.Text = input.Brand == HinoSmallPlate.Brand.HINO ? "Hino" : "Toyota";
            this.textEditCapacity.Text = input.Capacity;
            this.textEditModel.Text = input.Model;
            this.textEditColor.Text = input.Color;
            this.textEditEngine.Text = input.Engine;
            this.textEditFrame.Text = input.FrameNo;
            this.textEditPlant.Text = input.Plant;
            this.textEditTrans.Text = input.TransAxle;
            this.textEditTrim.Text = input.Trim;
        }
        #endregion
    }
}
