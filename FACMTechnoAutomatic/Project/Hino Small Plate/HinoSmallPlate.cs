﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TechnoSuiteRobot;

namespace FACMTechnoAutomatic
{
    class HinoSmallPlate : IFACMObjectClass
    {
        private HinoSmallPlateForm form;
        private MainMenu parent;
        internal HinoSmallPlate(MainMenu parent)
        {
            this.parent = parent;
        }
        internal enum Brand
        {
            TOYOTA, HINO
        }

        public string GetProjectName()
        {
            return "Hino Small Plate";
        }

        public Form GetInterfaceForm()
        {
            form = new HinoSmallPlateForm(parent);
            return form;
        }

        public int GetInputNumbers()
        {
            return 8;
        }

        public string GetDifferenceName()
        {
            return "HinoSmallPlateDifference";
        }

        public string GetPrimarySetName()
        {
            return "HinoSmallPlatePrimarySet";
        }

        public string GetParameterName()
        {
            return "HinoSmallPlateParameter";
        }
    }
}
