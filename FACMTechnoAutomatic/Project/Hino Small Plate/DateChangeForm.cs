﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FACMTechnoAutomatic
{
    public partial class DateChangeForm : Form
    {
        private HinoSmallPlateForm parent;
        public DateChangeForm(HinoSmallPlateForm parent)
        {
            InitializeComponent();
            this.parent = parent;
        }

        private void DateChangeForm_Load(object sender, EventArgs e)
        {

        }

        private void DateChangeForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.D1 || e.KeyCode == Keys.NumPad1)
            {
                parent.IncrementDate();
                parent.ResetMapping();
                Dispose();
            }
            else if (e.KeyCode == Keys.D2 || e.KeyCode == Keys.NumPad2)
            {
                parent.DecrementDate();
                parent.ResetMapping();
                Dispose();
            }
            else if (e.KeyCode == Keys.D3 || e.KeyCode == Keys.NumPad3)
                Dispose();
        }

        private void DateChangeForm_Load_1(object sender, EventArgs e)
        {

        }

    }
}
