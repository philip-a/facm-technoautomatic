﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FACMTechnoAutomatic
{
    class Plate
    {
        //0105103240101312099WU302R-TKMLHD3 01MHFC1JUX1B500466525T TP  0W9015CJ02779H4 RED  
        public HinoSmallPlate.Brand Brand { get; set; }
        public string Model {get; set;}
        public string Engine { get; set; }
        public string FrameNo { get; set; }
        public string Capacity { get; set; }
        public string Color { get; set; }
        public string Trim { get; set; }
        public string TransAxle { get; set; }
        public string Plant { get; set; }
        public string Spacing = " ";

        private string Trans;
        private string Axle;

        public Plate(string input) 
        {
            string[] inputs = input.Split(" ".ToCharArray());
            
            Model = GetModel(inputs[0]);
            Engine = GetEngine(Model);
            FrameNo = GetFrameNo(inputs[1]);
            Capacity = GetCapacity();
            Color = GetColor(inputs[4]);

            GetTrimAndTrans (Model, inputs[1].Substring(0,2));
            GetBrand(Model);

            TransAxle = Trans + Spacing + "/" + Spacing + Axle;
            Plant = GetPlant();
        }

        private string GetModel(string input) 
        {
            return input.Substring(19,input.Length - 19);
        }

        private string GetPlant()
        {
            return "Z 37";
        }
        
        private string GetEngine (string model) 
        {
            string engine = null;
            if (model.EndsWith("HD3")) 
            {
                engine = "W 04 D-TP";
                Trans = "M 153";
            }
            else if (model.EndsWith("JD3"))
            {
                engine = "W 04 D-TR";
                Trans = "M 550";
            }
            return engine;
        }

        private string GetFrameNo (string input) 
        {
            return input.Substring(2,17);
        }

        private string GetCapacity() 
        {
            return "4009";
        }

        private string GetColor (string input) 
        {
            string color = input.Substring(12,3);
            if (color == "999")
                color = "";
            return color;
        }

        private void GetTrimAndTrans (string model, string suffix) 
        {
            if (model == "WU302R-TKMLHD3")
            {
                if (suffix == "01" || suffix == "00" || suffix == "0I")
                    Trim = "LA";
                else if (suffix == "0J" || suffix == "0K")
                    Trim = "EA";
                else 
                    Trim = "LA";
                Axle = "A09A";
            }
            else if (model.StartsWith("WU302R-HKMLHD3"))
            {
                /*
                if (suffix == "01" || suffix == "00" || suffix == "0S" || suffix == "0P")
                {
                    Trim = "LA";
                }
                else 
                 */
                if (suffix == "0J" || suffix == "0K")
                {
                    Trim = "EA";
                }
                else if (suffix == "V2")
                {
                    Trim = "LB";
                }
                else
                {
                    Trim = "LA";
                }
                Axle = "A09A";
            }
            else if (model == "WU342R-TKMQHD3")
            {
                if (suffix == "01" || suffix == "00")
                    Trim = "LA";
                else if (suffix == "0M")
                    Trim = "EA";
                else
                    Trim = "LA";
                Axle = "A01A";
            }
            else if (model == "WU342R-HKMQHD3")
            {
                Trim = "LA";
                Axle = "A01A";
            }
            else if (model == "WU342R-TKMRHD3")
            {
                Trim = "LA";
                Axle = "A05A";
            }
            else if (model.StartsWith("WU342R-HKMRHD3"))
            {
                Trim = "LA";
                Axle = "A05A";
            }
            else if (model == "WU342L-HKMRHD3")
            {
                Trim = "LB";
                Axle = "A05A";
            }
            else if (model == "WU342R-TKMRJD3")
            {
                Trim = "LA";
                Axle = "A02A";
            }
            else if (model == "WU342R-HKMRJD3")
            {
                Trim = "LA";
                Axle = "A02A";
            }
            else if (model == "WU342R-TKMTJD3")
            {
                Trim = "LA";
                Axle = "B04A";
            }
            else if (model == "WU342R-HKMTJD3")
            {
                Trim = "LA";
                Axle = "B04A";
            }
            else if (model == "WU302L-HKMLHD3")
            {
                Trim = "LB";
                Axle = "A09A";
            }
            else if (model == "WU342L-HKMTHD3")
            {
                Trim = "LB";
                Axle = "B04A";
            }
            // This code is added on 21/09/2012
            if (suffix == "0G") Trans = "M YY5A";
        }
        
        private void GetBrand(string model) 
        {
            char prec = model[7];
            if (prec == 'T')
                Brand = HinoSmallPlate.Brand.TOYOTA;
            else if (prec == 'H')
                Brand = HinoSmallPlate.Brand.HINO;
        }
    }
}
