﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FACMTechnoAutomatic
{
    public partial class HinoSmallPlateInput : Form
    {
        private HinoSmallPlateForm parent;
        public HinoSmallPlateInput(HinoSmallPlateForm parent)
        {
            this.parent = parent;
            InitializeComponent();
        }

        private void simpleButtonOK_Click(object sender, EventArgs e)
        {
            int input;
            if (textEditSequence.Text == "")
                input = -1;
            else
                input = int.Parse(textEditSequence.Text);

            parent.SetSequence(input);

            Dispose();
        }


    }
}
