﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Threading;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SendKeys;
using TechnoSuiteRobot;
using DevExpress.XtraEditors;

namespace FACMTechnoAutomatic
{
    public partial class HinoSmallChasisForm : Form
    {
       #region Class Flag 
        private bool readyToPrint = false;
        private bool passwordTrue = false;
        private bool confirmPrint = false;
        private bool wantToStop = false;
        private bool shutDown = false;
        private bool isPrinting = false;
        private Thread WaitTillFinish;
        private delegate void SetTextCallback(TextEdit edit, string text);
        private delegate void SetStatusColorCallBack(Color color, string text);
        #endregion
       
        #region Fields
        private string input = null;
        private MainMenu parent;
        #endregion

        #region Resource Preload
        public HinoSmallChasisForm(MainMenu parent)
        {
            InitializeComponent();
            this.parent = parent;
        }

        private void HinoSmallChasisForm_Load(object sender, EventArgs e)
        {
            input = null;
            Console.WriteLine("Main Form Loaded, begin Garbage Collecting");
            GC.Collect();
        }
        #endregion
        
        #region Main Listener
        private void HinoSmallChasisForm_KeyDown(object sender, KeyEventArgs e)
        {
            /* STOP CRITERIA */
            if (e.KeyCode == Keys.NumPad7 || e.KeyCode == Keys.D7) 
            {
                if (!isPrinting)
                    return;

                Console.WriteLine("Attempting to stop the Program.");
                new ConfimationProceedForm(this, new Thread(new ThreadStart(SetStop)), "Want to Stop?").ShowDialog(this);

                if (wantToStop)
                {
                    int delay = FACMTechnoAutomatic.Properties.Settings.Default.Delay;
                    Console.WriteLine("Confirmed by user, preparing to stop TechnoMark.");
                    TechnoRobot stoper = new TechnoRobot(delay);
                    stoper.FocusTechnoSuite();
                    Thread.Sleep(200);
                    stoper.TechnoStop();
                    try { WaitTillFinish.Abort(); }
                    catch { }
                    Console.WriteLine("Finished sending command to stop");
                    Console.WriteLine("Begin to clear all fields and reactivate program");
                    isPrinting = false;
                    SetStatusColor(Color.Lime, "WAITING");
                    ResetField();
                    parent.FocusThis();
                    wantToStop = false;
                    Console.WriteLine("All set, Jobs are finished.");
                }
                else
                {
                    Console.WriteLine("Command is aborted.");
                }
                return;
            }

            /* CANNOT PROCESS OTHER INPUT WHILE PRINTING */
            if (isPrinting)
                return;

            /* ENTER BARCODE */
            if (e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.D1)
            {
                new HinoSmallChasisInput(this).ShowDialog(this);

                if (this.input != null)
                {
                    LoadSequence(this.input);
                    readyToPrint = true;
                }
                else
                {
                    MessageBox.Show("Input is not valid or has not embedded yet.", "Not Found",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ResetField();
                    readyToPrint = false;
                }
            }
            else if (e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.D4 || e.KeyCode == Keys.NumPad5 || e.KeyCode == Keys.D5)
            {
                bool isStart = false;
                if (e.KeyCode == Keys.D4 || e.KeyCode == Keys.NumPad4)
                    isStart = true;

                if (readyToPrint)
                {
                    string savedRecord = input;
                    DataTable dat = MainMenu.dataLayer.SelectQuery(
                        "select * from FACM_History where "+
                        "Sequence = '" + savedRecord +"'"
                    );

                    bool isPrinted = dat.Rows.Count != 0;
                    bool passwordEnable = FACMTechnoAutomatic.Properties.Settings.Default.Password;

                    IFACMObjectClass obj = parent.GetProject();

                    TechnoInput[] inps = new TechnoInput[obj.GetInputNumbers()];
                    DataTable prms = MainMenu.dataLayer.SelectQuery(
                        "select * from facm_" + obj.GetParameterName()
                    );

                    bool allLogoValid = CheckLogo(this.textEditType.Text);

                    if (!allLogoValid)
                    {
                        MessageBox.Show("There is some characters missing in logo sequence, Process cannot proceed.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }


                    if (textEditType.Text.Length != 17)
                    {
                        MessageBox.Show("Input is not valid.", "Not Valid", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (isPrinted && passwordEnable && isStart)
                    {
                        new SmallChasisPasswordForm(this).ShowDialog(this);
                        if (!passwordTrue)
                            return;
                    }
                    passwordTrue = false;

                    new ConfimationProceedForm(this, new Thread(new ThreadStart(SetConfirmationPrint)), "Ready to Print?").ShowDialog(this);

                    // executing phase
                    if (confirmPrint)
                    {
                        int delay = FACMTechnoAutomatic.Properties.Settings.Default.Delay;
                        TechnoSuiteRobot.TechnoRobot robot = new TechnoSuiteRobot.TechnoRobot(
                            FACMTechnoAutomatic.Properties.Settings.Default.LogoSequence,
                            FACMTechnoAutomatic.Properties.Settings.Default.MixedDefault,
                            delay
                        );

                        string[] messages = {
                            this.textEditType.Text
                        };
                        
                        for (int i = 0; i < obj.GetInputNumbers(); i++)
                        {
                            DataRow r = prms.Rows[i];

                            if (FACMTechnoAutomatic.Properties.Settings.Default.DefaultFont)
                                robot.StandardFont = true;

                            inps[i] = new TechnoInput(messages[i],
                                new TechnoParam(r["X"].ToString(), true),
                                new TechnoParam(r["Y"].ToString(), true),
                                new TechnoParam(r["Height"].ToString(), true),
                                new TechnoParam(r["Font"].ToString(), true),
                                new TechnoParam(r["Depth"].ToString(), true),
                                new TechnoParam(r["Angle"].ToString(), true),
                                new TechnoParam(r["Radius"].ToString(), true),
                                new TechnoParam(r["Compression"].ToString(), true),
                                new TechnoParam(r["Spacing"].ToString(), true),
                                new TechnoParam(r["Italic"].ToString(), true)
                            );
                        }

                        if (isStart)
                        {
                            Console.WriteLine("Executing Robot total input = " + inps.Length);
                            robot.ExecuteInput(inps, TechnoRobot.TechnoExecMode.START);
                            MainMenu.dataLayer.ExecuteQuery("insert into FACM_History values ('" +
                                EditDateTime(DateTime.Now) + "','"+ savedRecord + "','" +
                                EditDateTime(DateTime.Now) + "')"
                           );
                        }
                        else
                            robot.ExecuteInput(inps, TechnoRobot.TechnoExecMode.TEST);


                        WaitTillFinish = new Thread(new ThreadStart(PrintListener));
                        WaitTillFinish.Start();

                        parent.FocusThis();
                    }
                    GC.Collect();
                    confirmPrint = false;
                }
                else
                    MessageBox.Show("There is no input.", "Not Loaded", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                isStart = false;

            }
            else if (e.KeyCode == Keys.D8 || e.KeyCode == Keys.NumPad8)
            {
                new ConfimationProceedForm(this, new Thread(new ThreadStart(SetShutDown)), "Want to ShutDown?").ShowDialog(this);

                if (shutDown)
                    ApplicationManager.ShutDownComputer(parent.GetProject(), parent);
            }
        }

        #endregion

        #region Internal Interface Layer
        internal void SetMediumChasis(string input)
        {
            this.input = input;
        }

        internal void SetPasswordBoolean(bool pass)
        {
            this.passwordTrue = pass;
        }

        internal void SetConfirmationPrint()
        {
            this.confirmPrint = true;
        }

        internal void SetShutDown()
        {
            this.shutDown = true;
        }

        internal void SetStop()
        {
            this.wantToStop = true;
        }
        #endregion

        #region Date Change

        internal string EditDate(DateTime input)
        {
            int tanggal = input.Day;
            int bulan = input.Month;
            int tahun = input.Year;

            string date = (tanggal < 10 ? "0" : "") + tanggal.ToString();
            string month = (bulan < 10 ? "0" : "") + bulan.ToString();
            string year = tahun.ToString();

            return year + "/" + month + "/" + date;
        }

        internal string EditDateTime(DateTime input)
        {
            int hour = input.Hour;
            int minute = input.Minute;
            int second = input.Second;

            string hours = (hour < 10 ? "0" : "") + hour.ToString();
            string minutes = (minute < 10 ? "0" : "") + minute.ToString();
            string seconds = (second < 10 ? "0" : "") + second.ToString();

            return EditDate(input) + " " + hours + ":" + minutes + ":" + seconds;
        }

        private string ValidateDate(string inp)
        {
            if (inp.Length == 1)
                return "0" + inp;
            else
                return inp;
        }
        #endregion

        #region Local Routine
        private void PrintListener()
        {
            Console.WriteLine("Changing mode to printing");
            isPrinting = true;
            Console.WriteLine("Printing = " + isPrinting);
            SetStatusColor(Color.Red, "PRINTING");

            int sleep = FACMTechnoAutomatic.Properties.Settings.Default.PrintingTime;
            Console.WriteLine("Timer started, time = " + sleep + " miliseconds");
            Thread.Sleep(sleep);

            Console.WriteLine("Printing is done.");
            Console.WriteLine("Reseting all fields");
            ResetField();

            Console.WriteLine("Changing mode to waiting");
            isPrinting = false;
            SetStatusColor(Color.Lime, "WAITING");
            Console.WriteLine("Printing = "+ isPrinting);
            Console.WriteLine("Jobs done.");
        }

        private void SetText(TextEdit edit, string text)
        {
            if (edit.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { edit, text });
            }
            else
            {
                edit.Text = text;
            }
        }

        private void SetStatusColor(Color color, string text)
        {
            if (labelControlStatus.InvokeRequired)
            {
                SetStatusColorCallBack back = new SetStatusColorCallBack(SetStatusColor);
                this.Invoke(back, new object[] { color, text });
            }
            else
            {
                labelControlStatus.Text = text;
                labelControlStatus.ForeColor = color;
            }
        }

        private void ResetField()
        {
            input = null;
            SetText(textEditType, "");
            readyToPrint = false;
        }

        private bool CheckLogo(string inp)
        {
            string logoseq = FACMTechnoAutomatic.Properties.Settings.Default.LogoSequence;
            for (int i = 0; i < inp.Length; i++)
            {
                char x = inp[i];
                bool found = false;
                for (int j = 0; j < logoseq.Length; j++)
                {
                    if (x == logoseq[j])
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    return false;
            }
            return true;
        }

        private string ValidateColon(string input)
        {
            string ret = "";
            foreach (char c in input)
                if (c == ',')
                    ret += ".";
                else
                    ret += c;
            return ret;
        }

        private void LoadSequence(string input)
        {
            this.textEditType.Text = input;
        }
        #endregion
    }
}
