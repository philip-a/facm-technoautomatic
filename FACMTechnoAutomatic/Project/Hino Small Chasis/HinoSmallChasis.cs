﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TechnoSuiteRobot;

namespace FACMTechnoAutomatic
{
    class HinoSmallChasis : IFACMObjectClass
    {
        private Form form;
        private MainMenu parent;
        internal HinoSmallChasis(MainMenu parent)
        {
            this.parent = parent;
        }
        
        public string GetProjectName()
        {
            return "Hino Small Chasis";
        }

        public Form GetInterfaceForm()
        {
            form = new HinoSmallChasisForm(parent);
            return form;
        }

        public int GetInputNumbers()
        {
            return 1;
        }

        public string GetDifferenceName()
        {
            return "HinoSmallChasisDifference";
        }

        public string GetPrimarySetName()
        {
            return "HinoSmallChasisPrimarySet";
        }

        public string GetParameterName()
        {
            return "HinoSmallChasisParameter";
        }
    }
}
