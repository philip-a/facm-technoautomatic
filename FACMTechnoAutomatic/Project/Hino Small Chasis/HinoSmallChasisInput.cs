﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FACMTechnoAutomatic
{
    public partial class HinoSmallChasisInput : Form
    {
        private HinoSmallChasisForm parent;
        public HinoSmallChasisInput(HinoSmallChasisForm parent)
        {
            this.parent = parent;
            InitializeComponent();
        }

        private void simpleButtonOK_Click(object sender, EventArgs e)
        {
            parent.SetMediumChasis(textEditInput.Text);
            Dispose();
        }


    }
}
