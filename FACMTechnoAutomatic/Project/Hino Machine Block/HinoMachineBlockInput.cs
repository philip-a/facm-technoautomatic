﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FACMTechnoAutomatic
{
    public partial class HinoMachineBlockInput : Form
    {
        private HinoMachineBlockForm parent;
        public HinoMachineBlockInput(HinoMachineBlockForm parent)
        {
            this.parent = parent;
            InitializeComponent();
        }

        private void simpleButtonOK_Click(object sender, EventArgs e)
        {
            MachineBlock block = new MachineBlock();
            bool success = block.CreateInput(textEditInput.Text.ToUpper());
            parent.SetMachineBlock(success ? block : null);
            Dispose();
        }


    }
}
