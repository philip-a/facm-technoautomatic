﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FACMTechnoAutomatic
{
    class MachineBlock
    {
        public HinoMachineBlock.Type Type { get; set; }
        public string Prefix { get; set;}
        public string Middle { get; set; }
        public string Line   { get; set; }

        public MachineBlock() { }

        public string GetInput()
        {
            return Prefix + Middle + Line;
        }

        internal bool CreateInput(string inp)
        {
            if (CheckInput(inp))
            {
                Prefix = CreatePrefix(inp);
                Middle = CreateMiddle(inp);
                Line = CreateLine(inp);
                CreateType();
                return true;
            }
            return false;
        }

        private string CreatePrefix(string inp)
        {
            string pref = inp.Substring(0, 2);
            string ret = "";
            switch (pref)
            {
                case "PJ":
                case "RJ":
                case "NJ":
                case "FB":
                    ret += "W04D";
                    break;
                case "FJ":
                case "GJ":
                case "HJ":
                case "TJ":
                case "UJ":
                    ret += "J08E";
                    break;
            }
            return ret;
        }

        private string CreateMiddle(string inp)
        {
            string pref = inp.Substring(0, 2);
            string ret = "";
            switch (pref)
            {
                case "PJ":
                case "RJ":
                case "NJ":
                    ret += "T";
                    break;
                case "FB":
                    break;
                case "FJ":
                case "GJ":
                case "HJ":
                    ret += "U";
                    break;
                case "TJ":
                case "UJ":
                    ret += "V";
                    break; 
            }
            ret += pref;
            return ret;
        }

        private string CreateLine(string inp)
        {
            return inp.Substring(2);
        }

        private bool CheckInput(string inp)
        {
            if (inp.Length != 7)
            {
                return false;
            }
            string pref = inp.Substring(0, 2);
            bool check = false;
            switch (pref)
            {
                case "PJ":
                case "RJ":
                case "FJ":
                case "GJ":
                case "NJ":
                case "HJ":
                case "FB":
                case "TJ":
                case "UJ":
                    check = true;
                    break;
            }
            return check;
        }

        private void CreateType()
        {
            switch (this.Prefix)
            {
                case "W04D":
                    Type = HinoMachineBlock.Type.CYLINDER_4;
                    break;
                case "J08E":
                    Type = HinoMachineBlock.Type.CYLINDER_6;
                    break;
            }
        }
    }
}
