﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TechnoSuiteRobot;

namespace FACMTechnoAutomatic
{
    class HinoMachineBlock : IFACMObjectClass
    {
        private Form form;
        private MainMenu parent;
        internal HinoMachineBlock(MainMenu parent)
        {
            this.parent = parent;
        }
        internal enum Type
        {
            CYLINDER_4, CYLINDER_6
        }

        public string GetProjectName()
        {
            return "Hino Machine Block";
        }

        public Form GetInterfaceForm()
        {
            form = new HinoMachineBlockForm(parent);
            return form;
        }

        public int GetInputNumbers()
        {
            return 12;
        }

        public string GetDifferenceName()
        {
            return "HinoMachineBlockDifference";
        }

        public string GetPrimarySetName()
        {
            return "HinoMachineBlockPrimarySet";
        }

        public string GetParameterName()
        {
            return "HinoMachineBlockParameter";
        }
    }
}
