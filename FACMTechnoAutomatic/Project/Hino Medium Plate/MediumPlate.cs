﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FACMTechnoAutomatic
{
    class MediumPlate
    {
        public string FirstLine;
        public string SecondLine;

        public MediumPlate() { }

        public enum HMPIT
        {
            LINE1, LINE2
        }

        internal bool CreateInput(string inp, HMPIT type)
        {
            if (CheckInput(inp, type))
            {
                if (type == HMPIT.LINE1)
                {
                    FirstLine = inp;
                }
                else if (inp == "")
                {
                    SecondLine = "";
                    return true;
                }
                else
                {
                    int index = 0;
                    while (inp[index] != '-')
                    {
                        index++;
                    }
                    if (index == inp.Length || index < 2)
                    {
                        return false;
                    }
                    SecondLine = inp.Substring(index - 2);
                }
                return true;
            }
            return false;
        }

        private bool CheckInput(string inp, HMPIT type)
        {
            return (type == HMPIT.LINE1 ? inp.Length == 17 : true);
        }

    }
}
