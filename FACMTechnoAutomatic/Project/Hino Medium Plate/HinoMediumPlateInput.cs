﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FACMTechnoAutomatic
{
    public partial class HinoMediumPlateInput : Form
    {
        private HinoMediumPlateForm parent;
        private string secondLine = null;
        private MediumPlate.HMPIT mode;

        public HinoMediumPlateInput(HinoMediumPlateForm parent)
        {
            this.parent = parent;
            this.mode = MediumPlate.HMPIT.LINE1;
            InitializeComponent();
            this.layoutControlItemInput.Text = "ENTER First Line";
        }

        private HinoMediumPlateInput(HinoMediumPlateInput parent)
        {
            this.mode = MediumPlate.HMPIT.LINE2;
            InitializeComponent();
            this.layoutControlItemInput.Text = "ENTER Second Line";
        }


        private void simpleButtonOK_Click(object sender, EventArgs e)
        {
            if (mode == MediumPlate.HMPIT.LINE1)
            {
                MediumPlate mediumPlate = new MediumPlate();
                bool success = mediumPlate.CreateInput(textEditInput.Text.ToUpper(), MediumPlate.HMPIT.LINE1);
                bool nextLineSuccess = true;

                if (success)
                {
                    HinoMediumPlateInput next = new HinoMediumPlateInput(this);
                    next.ShowDialog(parent);
                    string nextLine = next.getSecondLine();
                    nextLineSuccess = mediumPlate.CreateInput(nextLine, MediumPlate.HMPIT.LINE2);
                }
                parent.SetMediumPlate(success && nextLineSuccess ? mediumPlate : null);
            }
            else
            {
                secondLine = textEditInput.Text.ToUpper();
            }
            Dispose();
        }

        internal string getSecondLine()
        {
            return secondLine;
        }
    }
}
