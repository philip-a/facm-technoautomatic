﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TechnoSuiteRobot;

namespace FACMTechnoAutomatic
{
    class HinoMediumPlate : IFACMObjectClass
    {
        private Form form;
        private MainMenu parent;
        internal HinoMediumPlate(MainMenu parent)
        {
            this.parent = parent;
        }

        public string GetProjectName()
        {
            return "Hino Medium Plate";
        }

        public Form GetInterfaceForm()
        {
            form = new HinoMediumPlateForm(parent);
            return form;
        }

        public int GetInputNumbers()
        {
            return 2;
        }

        public string GetDifferenceName()
        {
            return "HinoMediumPlateDifference";
        }

        public string GetPrimarySetName()
        {
            return "HinoMediumPlatePrimarySet";
        }

        public string GetParameterName()
        {
            return "HinoMediumPlateParameter";
        }
    }
}
