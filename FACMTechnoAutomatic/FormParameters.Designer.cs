﻿namespace FACMTechnoAutomatic
{
    partial class FormParameters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButtonExport = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonImport = new DevExpress.XtraEditors.SimpleButton();
            this.spinEditDelay = new DevExpress.XtraEditors.SpinEdit();
            this.lookUpEditOnSet = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEditOffSet = new DevExpress.XtraEditors.LookUpEdit();
            this.gridControlDifference = new DevExpress.XtraGrid.GridControl();
            this.gridViewDifference = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButtonExecute = new DevExpress.XtraEditors.SimpleButton();
            this.textEditConnectionString = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonSearch = new DevExpress.XtraEditors.SimpleButton();
            this.radioGroupPassword = new DevExpress.XtraEditors.RadioGroup();
            this.spinEditPrintingTime = new DevExpress.XtraEditors.SpinEdit();
            this.textEditExternalPath = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonLastSetting = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.textEditLogoSeq = new DevExpress.XtraEditors.TextEdit();
            this.radioGroupLogoDefault = new DevExpress.XtraEditors.RadioGroup();
            this.textEditMixedDefault = new DevExpress.XtraEditors.TextEdit();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridViewParameters = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemTextEditX = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDelay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditOnSet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditOffSet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDifference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDifference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditConnectionString.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPrintingTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditExternalPath.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLogoSeq.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupLogoDefault.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMixedDefault.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.simpleButtonExport);
            this.layoutControl1.Controls.Add(this.simpleButtonImport);
            this.layoutControl1.Controls.Add(this.spinEditDelay);
            this.layoutControl1.Controls.Add(this.lookUpEditOnSet);
            this.layoutControl1.Controls.Add(this.lookUpEditOffSet);
            this.layoutControl1.Controls.Add(this.gridControlDifference);
            this.layoutControl1.Controls.Add(this.simpleButtonExecute);
            this.layoutControl1.Controls.Add(this.textEditConnectionString);
            this.layoutControl1.Controls.Add(this.simpleButtonSearch);
            this.layoutControl1.Controls.Add(this.radioGroupPassword);
            this.layoutControl1.Controls.Add(this.spinEditPrintingTime);
            this.layoutControl1.Controls.Add(this.textEditExternalPath);
            this.layoutControl1.Controls.Add(this.simpleButtonLastSetting);
            this.layoutControl1.Controls.Add(this.simpleButtonSave);
            this.layoutControl1.Controls.Add(this.textEditLogoSeq);
            this.layoutControl1.Controls.Add(this.radioGroupLogoDefault);
            this.layoutControl1.Controls.Add(this.textEditMixedDefault);
            this.layoutControl1.Controls.Add(this.gridControl);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(137, 138, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(547, 340);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButtonExport
            // 
            this.simpleButtonExport.Location = new System.Drawing.Point(437, 306);
            this.simpleButtonExport.Name = "simpleButtonExport";
            this.simpleButtonExport.Size = new System.Drawing.Size(78, 22);
            this.simpleButtonExport.StyleController = this.layoutControl1;
            this.simpleButtonExport.TabIndex = 30;
            this.simpleButtonExport.Text = "Export";
            // 
            // simpleButtonImport
            // 
            this.simpleButtonImport.Location = new System.Drawing.Point(355, 306);
            this.simpleButtonImport.Name = "simpleButtonImport";
            this.simpleButtonImport.Size = new System.Drawing.Size(78, 22);
            this.simpleButtonImport.StyleController = this.layoutControl1;
            this.simpleButtonImport.TabIndex = 29;
            this.simpleButtonImport.Text = "Import";
            // 
            // spinEditDelay
            // 
            this.spinEditDelay.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditDelay.Location = new System.Drawing.Point(479, 12);
            this.spinEditDelay.Name = "spinEditDelay";
            this.spinEditDelay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditDelay.Properties.Mask.EditMask = "d";
            this.spinEditDelay.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spinEditDelay.Size = new System.Drawing.Size(56, 20);
            this.spinEditDelay.StyleController = this.layoutControl1;
            this.spinEditDelay.TabIndex = 28;
            // 
            // lookUpEditOnSet
            // 
            this.lookUpEditOnSet.Location = new System.Drawing.Point(380, 52);
            this.lookUpEditOnSet.Name = "lookUpEditOnSet";
            this.lookUpEditOnSet.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditOnSet.Size = new System.Drawing.Size(155, 20);
            this.lookUpEditOnSet.StyleController = this.layoutControl1;
            this.lookUpEditOnSet.TabIndex = 26;
            // 
            // lookUpEditOffSet
            // 
            this.lookUpEditOffSet.Location = new System.Drawing.Point(243, 52);
            this.lookUpEditOffSet.Name = "lookUpEditOffSet";
            this.lookUpEditOffSet.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditOffSet.Size = new System.Drawing.Size(133, 20);
            this.lookUpEditOffSet.StyleController = this.layoutControl1;
            this.lookUpEditOffSet.TabIndex = 25;
            this.lookUpEditOffSet.TextChanged += new System.EventHandler(this.lookUpEditOffSet_TextChanged);
            // 
            // gridControlDifference
            // 
            this.gridControlDifference.Location = new System.Drawing.Point(243, 102);
            this.gridControlDifference.MainView = this.gridViewDifference;
            this.gridControlDifference.Name = "gridControlDifference";
            this.gridControlDifference.Size = new System.Drawing.Size(292, 70);
            this.gridControlDifference.TabIndex = 24;
            this.gridControlDifference.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDifference});
            // 
            // gridViewDifference
            // 
            this.gridViewDifference.GridControl = this.gridControlDifference;
            this.gridViewDifference.Name = "gridViewDifference";
            this.gridViewDifference.OptionsView.ShowGroupPanel = false;
            // 
            // simpleButtonExecute
            // 
            this.simpleButtonExecute.Location = new System.Drawing.Point(380, 76);
            this.simpleButtonExecute.Name = "simpleButtonExecute";
            this.simpleButtonExecute.Size = new System.Drawing.Size(155, 22);
            this.simpleButtonExecute.StyleController = this.layoutControl1;
            this.simpleButtonExecute.TabIndex = 23;
            this.simpleButtonExecute.Text = "Execute";
            this.simpleButtonExecute.Click += new System.EventHandler(this.simpleButtonExecute_Click);
            // 
            // textEditConnectionString
            // 
            this.textEditConnectionString.Enabled = false;
            this.textEditConnectionString.Location = new System.Drawing.Point(331, 12);
            this.textEditConnectionString.Name = "textEditConnectionString";
            this.textEditConnectionString.Size = new System.Drawing.Size(51, 20);
            this.textEditConnectionString.StyleController = this.layoutControl1;
            this.textEditConnectionString.TabIndex = 27;
            // 
            // simpleButtonSearch
            // 
            this.simpleButtonSearch.Location = new System.Drawing.Point(243, 76);
            this.simpleButtonSearch.Name = "simpleButtonSearch";
            this.simpleButtonSearch.Size = new System.Drawing.Size(133, 22);
            this.simpleButtonSearch.StyleController = this.layoutControl1;
            this.simpleButtonSearch.TabIndex = 21;
            this.simpleButtonSearch.Text = "Search";
            this.simpleButtonSearch.Click += new System.EventHandler(this.simpleButtonSearch_Click);
            // 
            // radioGroupPassword
            // 
            this.radioGroupPassword.Location = new System.Drawing.Point(100, 41);
            this.radioGroupPassword.Name = "radioGroupPassword";
            this.radioGroupPassword.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "On"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Off")});
            this.radioGroupPassword.Size = new System.Drawing.Size(103, 25);
            this.radioGroupPassword.StyleController = this.layoutControl1;
            this.radioGroupPassword.TabIndex = 13;
            // 
            // spinEditPrintingTime
            // 
            this.spinEditPrintingTime.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditPrintingTime.Location = new System.Drawing.Point(100, 118);
            this.spinEditPrintingTime.Name = "spinEditPrintingTime";
            this.spinEditPrintingTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditPrintingTime.Properties.Mask.EditMask = "d";
            this.spinEditPrintingTime.Properties.MaxValue = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.spinEditPrintingTime.Size = new System.Drawing.Size(103, 20);
            this.spinEditPrintingTime.StyleController = this.layoutControl1;
            this.spinEditPrintingTime.TabIndex = 11;
            // 
            // textEditExternalPath
            // 
            this.textEditExternalPath.Location = new System.Drawing.Point(100, 142);
            this.textEditExternalPath.Name = "textEditExternalPath";
            this.textEditExternalPath.Size = new System.Drawing.Size(103, 20);
            this.textEditExternalPath.StyleController = this.layoutControl1;
            this.textEditExternalPath.TabIndex = 9;
            // 
            // simpleButtonLastSetting
            // 
            this.simpleButtonLastSetting.Location = new System.Drawing.Point(103, 306);
            this.simpleButtonLastSetting.Name = "simpleButtonLastSetting";
            this.simpleButtonLastSetting.Size = new System.Drawing.Size(69, 22);
            this.simpleButtonLastSetting.StyleController = this.layoutControl1;
            this.simpleButtonLastSetting.TabIndex = 15;
            this.simpleButtonLastSetting.Text = "Last Setting";
            this.simpleButtonLastSetting.Click += new System.EventHandler(this.simpleButtonLastSetting_Click);
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Location = new System.Drawing.Point(12, 306);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(71, 22);
            this.simpleButtonSave.StyleController = this.layoutControl1;
            this.simpleButtonSave.TabIndex = 14;
            this.simpleButtonSave.Text = "Save";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // textEditLogoSeq
            // 
            this.textEditLogoSeq.Location = new System.Drawing.Point(100, 70);
            this.textEditLogoSeq.Name = "textEditLogoSeq";
            this.textEditLogoSeq.Size = new System.Drawing.Size(103, 20);
            this.textEditLogoSeq.StyleController = this.layoutControl1;
            this.textEditLogoSeq.TabIndex = 6;
            // 
            // radioGroupLogoDefault
            // 
            this.radioGroupLogoDefault.Location = new System.Drawing.Point(12, 12);
            this.radioGroupLogoDefault.Name = "radioGroupLogoDefault";
            this.radioGroupLogoDefault.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Default Font"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Logo")});
            this.radioGroupLogoDefault.Size = new System.Drawing.Size(191, 25);
            this.radioGroupLogoDefault.StyleController = this.layoutControl1;
            this.radioGroupLogoDefault.TabIndex = 12;
            // 
            // textEditMixedDefault
            // 
            this.textEditMixedDefault.Location = new System.Drawing.Point(100, 94);
            this.textEditMixedDefault.Name = "textEditMixedDefault";
            this.textEditMixedDefault.Size = new System.Drawing.Size(103, 20);
            this.textEditMixedDefault.StyleController = this.layoutControl1;
            this.textEditMixedDefault.TabIndex = 7;
            // 
            // gridControl
            // 
            this.gridControl.Location = new System.Drawing.Point(12, 176);
            this.gridControl.MainView = this.gridViewParameters;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditX});
            this.gridControl.Size = new System.Drawing.Size(523, 126);
            this.gridControl.TabIndex = 4;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewParameters});
            // 
            // gridViewParameters
            // 
            this.gridViewParameters.GridControl = this.gridControl;
            this.gridViewParameters.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridViewParameters.Name = "gridViewParameters";
            this.gridViewParameters.OptionsView.ShowGroupPanel = false;
            this.gridViewParameters.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            // 
            // repositoryItemTextEditX
            // 
            this.repositoryItemTextEditX.AutoHeight = false;
            this.repositoryItemTextEditX.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditX.Name = "repositoryItemTextEditX";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem2,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.emptySpaceItem2,
            this.emptySpaceItem6,
            this.emptySpaceItem9,
            this.layoutControlItem15,
            this.layoutControlItem17,
            this.layoutControlItem12,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem13,
            this.emptySpaceItem4,
            this.layoutControlItem14,
            this.emptySpaceItem3,
            this.layoutControlItem16,
            this.layoutControlItem18,
            this.emptySpaceItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(547, 340);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 164);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(527, 130);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textEditLogoSeq;
            this.layoutControlItem3.CustomizationFormText = "Logo Sequence";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 58);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(195, 24);
            this.layoutControlItem3.Text = "Logo Sequence";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEditMixedDefault;
            this.layoutControlItem4.CustomizationFormText = "Mixed Default";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 82);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(195, 24);
            this.layoutControlItem4.Text = "Mixed Default";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEditExternalPath;
            this.layoutControlItem6.CustomizationFormText = "External Path";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 130);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(195, 24);
            this.layoutControlItem6.Text = "External Path";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.spinEditPrintingTime;
            this.layoutControlItem5.CustomizationFormText = "Printing Time";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 106);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(195, 24);
            this.layoutControlItem5.Text = "Printing Time";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.radioGroupLogoDefault;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(195, 29);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.radioGroupPassword;
            this.layoutControlItem2.CustomizationFormText = "Password";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 29);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(195, 29);
            this.layoutControlItem2.Text = "Password";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.simpleButtonSave;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 294);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(75, 26);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.simpleButtonLastSetting;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(91, 294);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(73, 26);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(507, 294);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(20, 26);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(75, 294);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(16, 26);
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(195, 0);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(36, 164);
            this.emptySpaceItem9.Text = "emptySpaceItem9";
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.simpleButtonSearch;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(231, 64);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(137, 26);
            this.layoutControlItem15.Text = "layoutControlItem15";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.simpleButtonExecute;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(368, 64);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(159, 26);
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.gridControlDifference;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(231, 90);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(296, 74);
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.lookUpEditOffSet;
            this.layoutControlItem10.CustomizationFormText = "Off Set";
            this.layoutControlItem10.Location = new System.Drawing.Point(231, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(137, 40);
            this.layoutControlItem10.Text = "Off Set";
            this.layoutControlItem10.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.lookUpEditOnSet;
            this.layoutControlItem11.CustomizationFormText = "On Set";
            this.layoutControlItem11.Location = new System.Drawing.Point(368, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(159, 40);
            this.layoutControlItem11.Text = "On Set";
            this.layoutControlItem11.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.textEditConnectionString;
            this.layoutControlItem13.CustomizationFormText = "Connection String";
            this.layoutControlItem13.Location = new System.Drawing.Point(231, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(143, 24);
            this.layoutControlItem13.Text = "Connection String";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(85, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 154);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(195, 10);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.spinEditDelay;
            this.layoutControlItem14.CustomizationFormText = "Delay";
            this.layoutControlItem14.Location = new System.Drawing.Point(435, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(92, 24);
            this.layoutControlItem14.Text = "Delay";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(27, 13);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(374, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(61, 24);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.simpleButtonImport;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(343, 294);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(82, 26);
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.simpleButtonExport;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(425, 294);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(82, 26);
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(164, 294);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(179, 26);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem1.Location = new System.Drawing.Point(188, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem4";
            this.emptySpaceItem1.Size = new System.Drawing.Size(115, 29);
            this.emptySpaceItem1.Text = "emptySpaceItem4";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem8.Location = new System.Drawing.Point(263, 29);
            this.emptySpaceItem8.Name = "emptySpaceItem4";
            this.emptySpaceItem8.Size = new System.Drawing.Size(264, 29);
            this.emptySpaceItem8.Text = "emptySpaceItem4";
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem10.Location = new System.Drawing.Point(263, 29);
            this.emptySpaceItem10.Name = "emptySpaceItem4";
            this.emptySpaceItem10.Size = new System.Drawing.Size(264, 29);
            this.emptySpaceItem10.Text = "emptySpaceItem4";
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem11.Location = new System.Drawing.Point(263, 29);
            this.emptySpaceItem11.Name = "emptySpaceItem4";
            this.emptySpaceItem11.Size = new System.Drawing.Size(264, 29);
            this.emptySpaceItem11.Text = "emptySpaceItem4";
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem12.Location = new System.Drawing.Point(263, 29);
            this.emptySpaceItem12.Name = "emptySpaceItem4";
            this.emptySpaceItem12.Size = new System.Drawing.Size(264, 29);
            this.emptySpaceItem12.Text = "emptySpaceItem4";
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem13.Location = new System.Drawing.Point(263, 29);
            this.emptySpaceItem13.Name = "emptySpaceItem4";
            this.emptySpaceItem13.Size = new System.Drawing.Size(264, 29);
            this.emptySpaceItem13.Text = "emptySpaceItem4";
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // FormParameters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 340);
            this.Controls.Add(this.layoutControl1);
            this.Name = "FormParameters";
            this.Text = "Parameters";
            this.Load += new System.EventHandler(this.XtraForm1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDelay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditOnSet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditOffSet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDifference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDifference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditConnectionString.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPrintingTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditExternalPath.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditLogoSeq.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupLogoDefault.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditMixedDefault.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewParameters;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit textEditLogoSeq;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit textEditMixedDefault;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit textEditExternalPath;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.SpinEdit spinEditPrintingTime;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.RadioGroup radioGroupPassword;
        private DevExpress.XtraEditors.RadioGroup radioGroupLogoDefault;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.SimpleButton simpleButtonLastSetting;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditX;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSearch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.SimpleButton simpleButtonExecute;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraGrid.GridControl gridControlDifference;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDifference;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditOffSet;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditOnSet;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.TextEdit textEditConnectionString;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.SpinEdit spinEditDelay;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.SimpleButton simpleButtonExport;
        private DevExpress.XtraEditors.SimpleButton simpleButtonImport;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;



    }
}