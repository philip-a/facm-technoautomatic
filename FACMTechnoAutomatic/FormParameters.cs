﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace FACMTechnoAutomatic
{
    public partial class FormParameters : DevExpress.XtraEditors.XtraForm
    {
        private bool LoadLastSetting = false;
        private MainMenu parent = null;
        private IFACMObjectClass application = null;
        public FormParameters(MainMenu parent, IFACMObjectClass obj)
        {
            application = obj;
            this.parent = parent;
            InitializeComponent();
        }

        private void LoadSetting()
        {
            this.textEditLogoSeq.Text = FACMTechnoAutomatic.Properties.Settings.Default.LogoSequence;
            this.textEditMixedDefault.Text = FACMTechnoAutomatic.Properties.Settings.Default.MixedDefault;
            this.textEditExternalPath.Text = FACMTechnoAutomatic.Properties.Settings.Default.ExternalPath;
            this.spinEditPrintingTime.Value = FACMTechnoAutomatic.Properties.Settings.Default.PrintingTime;
            this.textEditConnectionString.Text = FACMTechnoAutomatic.Properties.Settings.Default.ConnectionString;
            this.spinEditDelay.Value = FACMTechnoAutomatic.Properties.Settings.Default.Delay;
            this.lookUpEditOffSet.EditValue = null;
            this.lookUpEditOnSet.EditValue = null;
            this.lookUpEditOnSet.Properties.DataSource = null;
            this.gridControlDifference.DataSource = null;

            // LOGO or OCR
            bool printWithDefault = FACMTechnoAutomatic.Properties.Settings.Default.DefaultFont;
            if (printWithDefault)
                radioGroupLogoDefault.SelectedIndex = 0;
            else
                radioGroupLogoDefault.SelectedIndex = 1;

            // Password Request
            bool passwordPass = FACMTechnoAutomatic.Properties.Settings.Default.Password;
            if (passwordPass)
                this.radioGroupPassword.SelectedIndex = 0;
            else
                this.radioGroupPassword.SelectedIndex = 1;

            DataTable datView = MainMenu.dataLayer.SelectQuery(
                "select Name, X, Y, Height, Font, Depth, Angle, Radius, Compression, Spacing, Italic from FACM_" + 
                    application.GetParameterName()
            );

            this.gridControl.DataSource = datView;
            LoadDifference();
        }

        private void XtraForm1_Load(object sender, EventArgs e)
        {
            try
            {
                LoadSetting();
                LoadFeature();
                gridViewParameters.ShowingEditor += new CancelEventHandler(gridViewParameters_ShowingEditor);
                gridViewDifference.ShowingEditor += new CancelEventHandler(gridViewParameters_ShowingEditor);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LoadFeature()
        {
            this.simpleButtonImport.Enabled = parent.IsFeatureOn(MainMenu.Feature.SAVE_SETTING) ? true : false;
            this.simpleButtonExport.Enabled = parent.IsFeatureOn(MainMenu.Feature.SAVE_SETTING) ? true : false;
        }

        void gridViewParameters_ShowingEditor(object sender, CancelEventArgs e)
        {
            if ((sender as GridView).FocusedColumn.AbsoluteIndex == 0)
                e.Cancel = true;
        }

        private void simpleButtonSave_Click(object sender, EventArgs e)
        {
            FACMTechnoAutomatic.Properties.Settings.Default.LogoSequence = this.textEditLogoSeq.Text;
            FACMTechnoAutomatic.Properties.Settings.Default.ExternalPath = this.textEditExternalPath.Text;
            FACMTechnoAutomatic.Properties.Settings.Default.MixedDefault = this.textEditMixedDefault.Text;
            FACMTechnoAutomatic.Properties.Settings.Default.PrintingTime = (int)this.spinEditPrintingTime.Value;
            FACMTechnoAutomatic.Properties.Settings.Default.ConnectionString = this.textEditConnectionString.Text;
            FACMTechnoAutomatic.Properties.Settings.Default.Delay = (int)this.spinEditDelay.Value;

            if (radioGroupLogoDefault.SelectedIndex == 0)
                FACMTechnoAutomatic.Properties.Settings.Default.DefaultFont = true;
            else
                FACMTechnoAutomatic.Properties.Settings.Default.DefaultFont = false;

            if (radioGroupPassword.SelectedIndex == 0)
                FACMTechnoAutomatic.Properties.Settings.Default.Password = true;
            else
                FACMTechnoAutomatic.Properties.Settings.Default.Password = false;

            FACMTechnoAutomatic.Properties.Settings.Default.Save();

            string external_path = FACMTechnoAutomatic.Properties.Settings.Default.ExternalPath.Replace("\\", "#");

            MainMenu.dataLayer.ExecuteQuery(
                "update FACM_Active set " +
                "LogoSequence = '" + FACMTechnoAutomatic.Properties.Settings.Default.LogoSequence + "'," +
                "MixedDefault = '" + FACMTechnoAutomatic.Properties.Settings.Default.MixedDefault + "'," +
                "ExternalPath = '" + external_path + "'," +
                "PrintingTime = '" + FACMTechnoAutomatic.Properties.Settings.Default.PrintingTime + "'," +
                "Delay = '" + FACMTechnoAutomatic.Properties.Settings.Default.Delay + "'," +
                "Password = " + FACMTechnoAutomatic.Properties.Settings.Default.Password + "," +
                "DefaultFont = " + FACMTechnoAutomatic.Properties.Settings.Default.DefaultFont +
                " where project = '" +
                application.GetProjectName() + "'"
            );

            DataTable dat = GetDataTable(gridViewParameters);

            foreach (DataRow row in dat.Rows)
            {
                string query = "update FACM_"+application.GetParameterName()+" set " +
                    "X = " + FormatColon(row["X"].ToString()) + ", " +
                    "Y = " + FormatColon(row["Y"].ToString()) + ", " +
                    "Height = " + FormatColon(row["Height"].ToString()) + ", " +
                    "Font = " + row["Font"] + ", " +
                    "Depth = " + row["Depth"] + ", " +
                    "Angle = " + row["Angle"] + ", " +
                    "Radius = " + row["Radius"] + ", " +
                    "Compression = " + row["Compression"] + "," +
                    "Spacing = " + row["Spacing"] + "," +
                    "Italic = " + row["Italic"] + " " +
                    "where Name = '" + row["Name"] + "'";
                MainMenu.dataLayer.ExecuteQuery(query);
            }


            MessageBox.Show("Setting is saved successfully");
        }

        private void simpleButtonLastSetting_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure want to Load last setting?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                LoadLastSetting = true;
                LoadSetting();
            }
            LoadLastSetting = false;
        }

        private void LoadDifference()
        {
            DataTable dat = MainMenu.dataLayer.SelectQuery(
                "select BrandName from FACM_"+application.GetPrimarySetName()
            );

            lookUpEditOffSet.Properties.DataSource = dat;
            lookUpEditOffSet.Properties.DisplayMember = "BrandName";
        }

        private void simpleButtonSearch_Click(object sender, EventArgs e)
        {
            if (lookUpEditOnSet.Text == "" || lookUpEditOnSet.EditValue == null)
            {
                MessageBox.Show("Please Select On set member", "Not Selected", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            DataTable dat = MainMenu.dataLayer.SelectQuery(
                "select f.LineName, f.X, f.Y from FACM_Difference f, FACM_"+application.GetParameterName()+" k, FACM_HinoSmallPlatePrimarySet s where f.LineName = k.Name and " +
                "f.BrandName = s.BrandName and f.BrandName = '" + lookUpEditOnSet.Text + "'"
            );

            gridControlDifference.DataSource = dat;

        }

        private void lookUpEditOffSet_TextChanged(object sender, EventArgs e)
        {
            if (LoadLastSetting)
            {
                return;
            }
            FACMTechnoAutomatic.Properties.Settings.Default.OffSet = lookUpEditOffSet.Text;
            gridControlDifference.DataSource = null;

            MainMenu.dataLayer.ExecuteQuery(
                "update FACM_"+application.GetPrimarySetName()+" set PrimarySet = 1 where BrandName = '" +
                lookUpEditOffSet.Text + "'"
            );
            MainMenu.dataLayer.ExecuteQuery(
                "update FACM_"+application.GetPrimarySetName()+" set PrimarySet = 0 where BrandName <> '" +
                lookUpEditOffSet.Text + "'"
            );

            DataTable dat = MainMenu.dataLayer.SelectQuery(
                "select BrandName from FACM_"+application.GetPrimarySetName()+" where PrimarySet = 0"
            );

            lookUpEditOnSet.Properties.DataSource = dat;
            lookUpEditOnSet.Properties.DisplayMember = "BrandName";
        }

        private void simpleButtonExecute_Click(object sender, EventArgs e)
        {
            if (lookUpEditOffSet.EditValue == null)
            {
                MessageBox.Show("Please Select Off set Member", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (lookUpEditOnSet.EditValue == null)
            {
                MessageBox.Show("Please select on set member", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int rowCount = gridViewDifference.DataRowCount;
            DataTable dat = GetDataTable(gridViewDifference);

            foreach (DataRow row in dat.Rows)
            {
                string query = "update FACM_Difference set X = " +
                    FormatColon(row["X"].ToString()) + ", Y = " + FormatColon(row["Y"].ToString()) + " where BrandName = '" +
                    lookUpEditOnSet.Text + "' and LineName = '" + row["LineName"] +
                    "'";
                MainMenu.dataLayer.ExecuteQuery(query);
            }

            MessageBox.Show("Update Successfull", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private DataTable GetDataTable(GridView input)
        {
            DataTable dat = new DataTable();
            int rowCount = input.DataRowCount;

            foreach (GridColumn col in input.Columns)
                dat.Columns.Add(col.Name.Substring(3,col.Name.Length -3), typeof(string));

            for (int i = 0; i < rowCount; i++)
            {
                object[] param = new object[dat.Columns.Count];
                
                int ctr = 0;
                foreach (GridColumn col in input.Columns)
                    param[ctr++] = input.GetRowCellDisplayText(i, col);

                dat.Rows.Add(param);
            }
            return dat;
        }

        private string FormatColon(string input)
        {
            string ret = "";
            foreach (char c in input)
                if (c == ',')
                    ret += ".";
                else
                    ret += c;
            return ret;

        }
    }
}