﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechnoSuiteRobot;
using System.Windows.Forms;

namespace FACMTechnoAutomatic
{
    public interface IFACMObjectClass
    {
        string GetProjectName();
        Form GetInterfaceForm();
        int GetInputNumbers();
        string GetDifferenceName();
        string GetPrimarySetName();
        string GetParameterName();
    }
}
