﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FACMTechnoAutomatic
{
    public partial class PasswordRequest : Form
    {
        public PasswordRequest()
        {
            InitializeComponent();
        }

        private void PasswordRequest_Load(object sender, EventArgs e)
        {

        }

        private void simpleButtonEnter_Click(object sender, EventArgs e)
        {
            DataTable dat = MainMenu.dataLayer.SelectQuery(
                "select password from FACM_Account where username = 'Super Administrator' or username = 'Administrator'"
            );

            foreach(DataRow row in dat.Rows) 
                if (row["password"].ToString() == textEditPassword.Text) 
                {
                    MainMenu.admin = true;
                    break;
                }

            if (!MainMenu.admin)
            {
                MessageBox.Show("Wrong Password", "Wrong Password", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Dispose();
        }

        
    }
}
