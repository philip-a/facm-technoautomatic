﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace FACMTechnoAutomatic
{
    public partial class ConfimationProceedForm : Form
    {
        private Form parent;
        private Thread thread;
        public ConfimationProceedForm(Form parent, Thread method, String Text)
        {
            this.thread = method;
            InitializeComponent();
            this.parent = parent;
            this.labelControlTitle.Text = Text;
        }

        private void ConfimationProceedForm_Load(object sender, EventArgs e)
        {

        }

        private void ConfimationProceedForm_KeyDown(object sender, KeyEventArgs e)
        {
            Cursor.Position = new Point(Screen.PrimaryScreen.Bounds.Width / 2,
                            Screen.PrimaryScreen.Bounds.Height / 2);
            if (e.KeyCode == Keys.D1 || e.KeyCode == Keys.NumPad1)
            {
                thread.Start();
                while (thread.IsAlive);
                Dispose();
            }
            else if (e.KeyCode == Keys.D2 || e.KeyCode == Keys.NumPad2)
                Dispose();
        }

        private void labelControl1_Click(object sender, EventArgs e)
        {
            thread.Start();
            while (thread.IsAlive) ;
            Dispose();
        }

        private void labelControl2_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
