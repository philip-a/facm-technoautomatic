﻿/* By Philip Arthur
 * 16th August 2011
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;


namespace MySqlDataManager
{
    public sealed class MySqlDataLayer
    {
        #region Field
        public string ConnectionString { get; set; }
        #endregion
        
        #region Constructors
        public MySqlDataLayer() { }
        public MySqlDataLayer(string connectionString)
        {
            this.ConnectionString = connectionString;
        }
        #endregion

        #region Stored Procedure
        public bool ExecuteVoidSP(string storedProcedure, MySqlSPParam[] parameters) 
        {
            using (MySqlConnection con = new MySqlConnection(ConnectionString))
            {
                con.Open();
                MySqlTransaction trans = con.BeginTransaction();
                MySqlCommand cmd = new MySqlCommand(storedProcedure, con, trans);
                cmd.CommandType = CommandType.StoredProcedure;

                if (parameters != null)
                foreach (MySqlSPParam parameter in parameters)
                    cmd.Parameters.Add(new MySqlParameter(parameter.ParamName, parameter.ParamValue));

                // Begin to Execute Stored Procedure
                try
                {
                    cmd.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    /* This Code was copied from 
                     * http://msdn.microsoft.com/en-us/library/system.data.sqlclient.MySqlTransaction.aspx
                     */

                    Console.WriteLine("Commit Exception Type: {0}", ex.GetType());
                    Console.WriteLine("  Message: {0}", ex.Message);

                    // Attempt to roll back the transaction.
                    try
                    {
                        trans.Rollback();
                    }
                    catch (Exception exc)
                    {
                        // This catch block will handle any errors that may have occurred
                        // on the server that would cause the rollback to fail, such as
                        // a closed connection.
                        Console.WriteLine("Rollback Exception Type: {0}", exc.GetType());
                        Console.WriteLine("  Message: {0}", exc.Message);
                        throw (exc);
                    }
                    return false;
                }
            }
            return true;
        }

        public bool ExecuteVoidSP(MySqlSP storedProcedure)
        {
            return ExecuteVoidSP(new MySqlSP[] { storedProcedure });
        }

        public bool ExecuteVoidSP(MySqlSP[] storedProcedures)
        {
            using (MySqlConnection con = new MySqlConnection(ConnectionString))
            {
                con.Open();
                MySqlTransaction trans = con.BeginTransaction();

                foreach (MySqlSP sp in storedProcedures)
                {
                    MySqlCommand cmd = new MySqlCommand(sp.SPName, con, trans);
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (sp.SPParam != null)
                        foreach (MySqlSPParam parameter in sp.SPParam)
                            cmd.Parameters.Add(new MySqlParameter(parameter.ParamName, parameter.ParamValue));

                    // Begin to Execute Stored Procedure
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        /* This Code was copied from 
                         * http://msdn.microsoft.com/en-us/library/system.data.sqlclient.MySqlTransaction.aspx
                         */

                        Console.WriteLine("Commit Exception Type: {0}", ex.GetType());
                        Console.WriteLine("  Message: {0}", ex.Message);

                        // Attempt to roll back the transaction.
                        try
                        {
                            trans.Rollback();
                        }
                        catch (Exception exc)
                        {
                            // This catch block will handle any errors that may have occurred
                            // on the server that would cause the rollback to fail, such as
                            // a closed connection.
                            Console.WriteLine("Rollback Exception Type: {0}", exc.GetType());
                            Console.WriteLine("  Message: {0}", exc.Message);
                            throw (exc);
                        }
                        return false;
                    }
                }
                trans.Commit();
            }
            return true;
        }

        public DataTable ExecuteSelectSP(MySqlSP storedProcedure)
        {
            return ExecuteSelectSP(storedProcedure.SPName, storedProcedure.SPParam);
        }

        public DataTable ExecuteSelectSP(string storedProcedure, MySqlSPParam[] parameters)
        {
            DataTable dat = new DataTable();
            using (MySqlConnection con = new MySqlConnection(ConnectionString))
            {
                MySqlCommand cmd = new MySqlCommand(storedProcedure, con);
                cmd.CommandType = CommandType.StoredProcedure;

                if (parameters != null)
                    foreach (MySqlSPParam parameter in parameters)
                        cmd.Parameters.Add(new MySqlParameter(parameter.ParamName, parameter.ParamValue));

                using (MySqlDataAdapter adapt = new MySqlDataAdapter(cmd))
                {
                    try
                    {
                        adapt.Fill(dat);
                    }
                    catch (MySqlException exp)
                    {
                        Console.WriteLine("Select Exception Type: {0}", exp.GetType());
                        Console.WriteLine("  Message: {0}", exp.Message);
                        throw (exp);
                    }
                }
            }
            return dat;
        }
        #endregion

        #region Another Methods

        public bool IsConnected()
        {
            if (ConnectionString == null)
                return false;
            else
            {
                MySqlConnection con = new MySqlConnection(ConnectionString);
                bool connect = false;
                try
                {
                    con.Open();
                    connect = true;
                }
                catch (MySqlException ex)
                {
                    ex.ToString();
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }

                if (!connect)
                    Console.WriteLine("Connection set Failed.");
                else
                    Console.WriteLine("Connection set Successful.:");
                
                return connect;
            }
        }
        #endregion

        #region Querry Executing
        public string ExecuteScalarQuery (string query) 
        {
            string ret = null;
            try
            {
                using (MySqlConnection con = new MySqlConnection(ConnectionString))
                {
                    con.Open();
                    MySqlCommand cmd = new MySqlCommand(query, con);
                    ret = cmd.ExecuteScalar().ToString();
                }
            }
            catch (MySqlException exp)
            {
                Console.WriteLine("Select Exception Type: {0}", exp.GetType());
                Console.WriteLine("  Message: {0}", exp.Message);
                throw (exp);
            }
            return ret;
        }
        
        public DataTable SelectQuery(string query)
        {
            DataTable dat = new DataTable();
            try
            {
                using (MySqlConnection con = new MySqlConnection(ConnectionString))
                {
                    con.Open();
                    using (MySqlDataAdapter da = new MySqlDataAdapter(query, con))
                    {
                        da.Fill(dat);
                    }
                }
            }
            catch (MySqlException exp)
            {
                MessageBox.Show(exp.ToString());
                throw exp;
            }
            finally
            {
                
            }
            return dat;
        }

        public bool ExecuteQuery(string param_query)
        {
            string[] par = { param_query };
            return ExecuteQuery(par);
        }

        public bool ExecuteQuery(string[] param_query)
        {
            try
            {
                using (MySqlConnection sqlCon = new MySqlConnection(ConnectionString))
                {
                    try
                    {
                        // attempt to open connection
                        sqlCon.Open();
                    }
                    catch (MySqlException ex)
                    {
                        // failed to connect
                        ex.ToString();
                        return false;
                    }

                    MySqlCommand command = sqlCon.CreateCommand();

                    // Begin Transaction
                    MySqlTransaction transaction = sqlCon.BeginTransaction();

                    command.Connection = sqlCon;
                    command.Transaction = transaction;

                    foreach (string param in param_query)
                    {
                        // Begin Executing 
                        try
                        {
                            command.CommandText = param;
                            command.ExecuteNonQuery();
                        }
                        catch (Exception except)
                        {
                            /* This Code was copied from 
                             * http://msdn.microsoft.com/en-us/library/system.data.sqlclient.MySqlTransaction.aspx
                             */

                            Console.WriteLine("Commit Exception Type: {0}", except.GetType());
                            Console.WriteLine("  Message: {0}", except.Message);

                            // Attempt to roll back the transaction.
                            try
                            {
                                transaction.Rollback();
                            }
                            catch (Exception exc)
                            {
                                // This catch block will handle any errors that may have occurred
                                // on the server that would cause the rollback to fail, such as
                                // a closed connection.
                                Console.WriteLine("Rollback Exception Type: {0}", exc.GetType());
                                Console.WriteLine("  Message: {0}", exc.Message);
                            }
                            return false;
                        } 
                    }
                    // Attempt to commit the transaction.
                    transaction.Commit();

                    // Success to execute query
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                return false;
            }
        }
        #endregion
    }
}
