﻿using System;
using System.Linq;
using System.Text;

namespace MySqlDataManager
{
    public sealed class MySqlSPParam
    {
        public MySqlSPParam(string paramName, string paramValue)
        {
            ParamName = "@" + paramName;
            ParamValue = paramValue;
        }
        public string ParamName { get; set; }
        public string ParamValue { get; set; }
    }
}
