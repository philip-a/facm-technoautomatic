﻿using System;
using System.Linq;
using System.Text;
using System.Collections;

namespace MySqlDataManager
{
    public sealed class MySqlSP
    {
        public MySqlSP(string SPName, MySqlSPParam[] parameter)
        {
            this.SPName = SPName;
            this.SPParam = parameter;
        }

        public string SPName { get; set; }
        public MySqlSPParam[] SPParam { get; set; }
    }
}
