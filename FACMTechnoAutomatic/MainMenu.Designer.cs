﻿namespace FACMTechnoAutomatic
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.defaultLookAndFeel = new DevExpress.LookAndFeel.DefaultLookAndFeel();
            this.layoutControlMainMenu = new DevExpress.XtraLayout.LayoutControl();
            this.labelControlVersion = new DevExpress.XtraEditors.LabelControl();
            this.radioGroup = new DevExpress.XtraEditors.RadioGroup();
            this.barManagerMainMenu = new DevExpress.XtraBars.BarManager();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barStaticItemVersion = new DevExpress.XtraBars.BarStaticItem();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.panelControlMain = new DevExpress.XtraEditors.PanelControl();
            this.layoutControlPanel = new DevExpress.XtraLayout.LayoutControl();
            this.labelControlFor = new DevExpress.XtraEditors.LabelControl();
            this.pictureEditTechno = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlGroupPanel = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemPicture = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.navBarControlMain = new DevExpress.XtraNavBar.NavBarControl();
            this.navBarGroupAdmin = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItemParameters = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItemPassword = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItemHistory = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroupProgram = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItemMain = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItemExit = new DevExpress.XtraNavBar.NavBarItem();
            this.layoutControlMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemNavBar = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPanel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemVersion = new DevExpress.XtraLayout.LayoutControlItem();
            this.imageCollection = new DevExpress.Utils.ImageCollection();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMainMenu)).BeginInit();
            this.layoutControlMainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManagerMainMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlMain)).BeginInit();
            this.panelControlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPanel)).BeginInit();
            this.layoutControlPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditTechno.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControlMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNavBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // defaultLookAndFeel
            // 
            this.defaultLookAndFeel.LookAndFeel.SkinName = "Money Twins";
            // 
            // layoutControlMainMenu
            // 
            this.layoutControlMainMenu.Controls.Add(this.labelControlVersion);
            this.layoutControlMainMenu.Controls.Add(this.radioGroup);
            this.layoutControlMainMenu.Controls.Add(this.panelControlMain);
            this.layoutControlMainMenu.Controls.Add(this.navBarControlMain);
            this.layoutControlMainMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.layoutControlMainMenu.Location = new System.Drawing.Point(0, 0);
            this.layoutControlMainMenu.Name = "layoutControlMainMenu";
            this.layoutControlMainMenu.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(465, 194, 250, 350);
            this.layoutControlMainMenu.Root = this.layoutControlMain;
            this.layoutControlMainMenu.Size = new System.Drawing.Size(180, 402);
            this.layoutControlMainMenu.TabIndex = 1;
            this.layoutControlMainMenu.Text = "layoutControl";
            // 
            // labelControlVersion
            // 
            this.labelControlVersion.Location = new System.Drawing.Point(2, 387);
            this.labelControlVersion.Name = "labelControlVersion";
            this.labelControlVersion.Size = new System.Drawing.Size(74, 13);
            this.labelControlVersion.StyleController = this.layoutControlMainMenu;
            this.labelControlVersion.TabIndex = 7;
            this.labelControlVersion.Text = "Version x.x.x.x";
            this.labelControlVersion.Click += new System.EventHandler(this.labelControlVersion_Click);
            // 
            // radioGroup
            // 
            this.radioGroup.Location = new System.Drawing.Point(2, 340);
            this.radioGroup.MenuManager = this.barManagerMainMenu;
            this.radioGroup.Name = "radioGroup";
            this.radioGroup.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "Admin"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Operator")});
            this.radioGroup.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radioGroup.Size = new System.Drawing.Size(176, 43);
            this.radioGroup.StyleController = this.layoutControlMainMenu;
            this.radioGroup.TabIndex = 6;
            this.radioGroup.SelectedIndexChanged += new System.EventHandler(this.radioGroup_SelectedIndexChanged);
            // 
            // barManagerMainMenu
            // 
            this.barManagerMainMenu.DockControls.Add(this.barDockControlTop);
            this.barManagerMainMenu.DockControls.Add(this.barDockControlBottom);
            this.barManagerMainMenu.DockControls.Add(this.barDockControlLeft);
            this.barManagerMainMenu.DockControls.Add(this.barDockControlRight);
            this.barManagerMainMenu.Form = this;
            this.barManagerMainMenu.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItemVersion});
            this.barManagerMainMenu.MaxItemId = 8;
            this.barManagerMainMenu.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemDateEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit3});
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(732, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 402);
            this.barDockControlBottom.Size = new System.Drawing.Size(732, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 402);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(732, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 402);
            // 
            // barStaticItemVersion
            // 
            this.barStaticItemVersion.Caption = "V 1.0.0.0";
            this.barStaticItemVersion.Id = 4;
            this.barStaticItemVersion.Name = "barStaticItemVersion";
            this.barStaticItemVersion.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // panelControlMain
            // 
            this.panelControlMain.Controls.Add(this.layoutControlPanel);
            this.panelControlMain.Location = new System.Drawing.Point(0, 0);
            this.panelControlMain.Name = "panelControlMain";
            this.panelControlMain.Size = new System.Drawing.Size(180, 171);
            this.panelControlMain.TabIndex = 5;
            // 
            // layoutControlPanel
            // 
            this.layoutControlPanel.Controls.Add(this.labelControlFor);
            this.layoutControlPanel.Controls.Add(this.pictureEditTechno);
            this.layoutControlPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlPanel.Location = new System.Drawing.Point(2, 2);
            this.layoutControlPanel.Name = "layoutControlPanel";
            this.layoutControlPanel.Root = this.layoutControlGroupPanel;
            this.layoutControlPanel.Size = new System.Drawing.Size(176, 167);
            this.layoutControlPanel.TabIndex = 0;
            this.layoutControlPanel.Text = "layoutControl1";
            // 
            // labelControlFor
            // 
            this.labelControlFor.Location = new System.Drawing.Point(2, 2);
            this.labelControlFor.Name = "labelControlFor";
            this.labelControlFor.Size = new System.Drawing.Size(172, 13);
            this.labelControlFor.StyleController = this.layoutControlPanel;
            this.labelControlFor.TabIndex = 5;
            // 
            // pictureEditTechno
            // 
            this.pictureEditTechno.EditValue = global::FACMTechnoAutomatic.Properties.Resources.TECHNOMARK_LOGO1;
            this.pictureEditTechno.Location = new System.Drawing.Point(2, 19);
            this.pictureEditTechno.Name = "pictureEditTechno";
            this.pictureEditTechno.Properties.InitialImage = null;
            this.pictureEditTechno.Size = new System.Drawing.Size(172, 146);
            this.pictureEditTechno.StyleController = this.layoutControlPanel;
            this.pictureEditTechno.TabIndex = 4;
            // 
            // layoutControlGroupPanel
            // 
            this.layoutControlGroupPanel.CustomizationFormText = "Root";
            this.layoutControlGroupPanel.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroupPanel.GroupBordersVisible = false;
            this.layoutControlGroupPanel.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemPicture,
            this.layoutControlItem1});
            this.layoutControlGroupPanel.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupPanel.Name = "Root";
            this.layoutControlGroupPanel.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroupPanel.Size = new System.Drawing.Size(176, 167);
            this.layoutControlGroupPanel.Text = "Root";
            this.layoutControlGroupPanel.TextVisible = false;
            // 
            // layoutControlItemPicture
            // 
            this.layoutControlItemPicture.Control = this.pictureEditTechno;
            this.layoutControlItemPicture.CustomizationFormText = "layoutControlItemPicture";
            this.layoutControlItemPicture.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItemPicture.Name = "layoutControlItemPicture";
            this.layoutControlItemPicture.Size = new System.Drawing.Size(176, 150);
            this.layoutControlItemPicture.Text = "layoutControlItemPicture";
            this.layoutControlItemPicture.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemPicture.TextToControlDistance = 0;
            this.layoutControlItemPicture.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.labelControlFor;
            this.layoutControlItem1.CustomizationFormText = "Designed for:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(176, 17);
            this.layoutControlItem1.Text = "Designed for:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // navBarControlMain
            // 
            this.navBarControlMain.ActiveGroup = this.navBarGroupAdmin;
            this.navBarControlMain.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroupAdmin,
            this.navBarGroupProgram});
            this.navBarControlMain.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.navBarItemParameters,
            this.navBarItemPassword,
            this.navBarItemMain,
            this.navBarItemExit,
            this.navBarItemHistory});
            this.navBarControlMain.Location = new System.Drawing.Point(2, 173);
            this.navBarControlMain.Name = "navBarControlMain";
            this.navBarControlMain.OptionsNavPane.ExpandedWidth = 176;
            this.navBarControlMain.Size = new System.Drawing.Size(176, 163);
            this.navBarControlMain.TabIndex = 4;
            this.navBarControlMain.Text = "navBarControl1";
            // 
            // navBarGroupAdmin
            // 
            this.navBarGroupAdmin.Caption = "Administrator";
            this.navBarGroupAdmin.Expanded = true;
            this.navBarGroupAdmin.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItemParameters),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItemPassword),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItemHistory)});
            this.navBarGroupAdmin.Name = "navBarGroupAdmin";
            // 
            // navBarItemParameters
            // 
            this.navBarItemParameters.Caption = "Parameters";
            this.navBarItemParameters.Name = "navBarItemParameters";
            // 
            // navBarItemPassword
            // 
            this.navBarItemPassword.Caption = "Password";
            this.navBarItemPassword.Name = "navBarItemPassword";
            // 
            // navBarItemHistory
            // 
            this.navBarItemHistory.Caption = "History";
            this.navBarItemHistory.Name = "navBarItemHistory";
            this.navBarItemHistory.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItemHistory_LinkClicked);
            // 
            // navBarGroupProgram
            // 
            this.navBarGroupProgram.Caption = "Program";
            this.navBarGroupProgram.Expanded = true;
            this.navBarGroupProgram.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItemMain),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItemExit)});
            this.navBarGroupProgram.Name = "navBarGroupProgram";
            // 
            // navBarItemMain
            // 
            this.navBarItemMain.Caption = "Main";
            this.navBarItemMain.Name = "navBarItemMain";
            // 
            // navBarItemExit
            // 
            this.navBarItemExit.Caption = "Exit";
            this.navBarItemExit.Name = "navBarItemExit";
            this.navBarItemExit.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItemExit_LinkClicked);
            // 
            // layoutControlMain
            // 
            this.layoutControlMain.CustomizationFormText = "Root";
            this.layoutControlMain.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlMain.GroupBordersVisible = false;
            this.layoutControlMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemNavBar,
            this.layoutControlItemPanel,
            this.layoutControlItem2,
            this.layoutControlItemVersion});
            this.layoutControlMain.Location = new System.Drawing.Point(0, 0);
            this.layoutControlMain.Name = "Root";
            this.layoutControlMain.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlMain.Size = new System.Drawing.Size(180, 402);
            this.layoutControlMain.Text = "Root";
            this.layoutControlMain.TextVisible = false;
            // 
            // layoutControlItemNavBar
            // 
            this.layoutControlItemNavBar.Control = this.navBarControlMain;
            this.layoutControlItemNavBar.CustomizationFormText = "layoutControlItemNavBar";
            this.layoutControlItemNavBar.Location = new System.Drawing.Point(0, 171);
            this.layoutControlItemNavBar.Name = "layoutControlItemNavBar";
            this.layoutControlItemNavBar.Size = new System.Drawing.Size(180, 167);
            this.layoutControlItemNavBar.Text = "layoutControlItemNavBar";
            this.layoutControlItemNavBar.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemNavBar.TextToControlDistance = 0;
            this.layoutControlItemNavBar.TextVisible = false;
            // 
            // layoutControlItemPanel
            // 
            this.layoutControlItemPanel.Control = this.panelControlMain;
            this.layoutControlItemPanel.CustomizationFormText = "layoutControlItemPanel";
            this.layoutControlItemPanel.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemPanel.Name = "layoutControlItemPanel";
            this.layoutControlItemPanel.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlItemPanel.Size = new System.Drawing.Size(180, 171);
            this.layoutControlItemPanel.Text = "layoutControlItemPanel";
            this.layoutControlItemPanel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemPanel.TextToControlDistance = 0;
            this.layoutControlItemPanel.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.radioGroup;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 338);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(180, 47);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItemVersion
            // 
            this.layoutControlItemVersion.Control = this.labelControlVersion;
            this.layoutControlItemVersion.CustomizationFormText = "layoutControlItemVersion";
            this.layoutControlItemVersion.Location = new System.Drawing.Point(0, 385);
            this.layoutControlItemVersion.Name = "layoutControlItemVersion";
            this.layoutControlItemVersion.Size = new System.Drawing.Size(180, 17);
            this.layoutControlItemVersion.Text = "layoutControlItemVersion";
            this.layoutControlItemVersion.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemVersion.TextToControlDistance = 0;
            this.layoutControlItemVersion.TextVisible = false;
            // 
            // imageCollection
            // 
            this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection.ImageStream")));
            this.imageCollection.Images.SetKeyName(0, "button_green.jpg");
            this.imageCollection.Images.SetKeyName(1, "button_red.jpg");
            this.imageCollection.Images.SetKeyName(2, "zoom_98886TechnoMark_logo_v3-fondblanc-1600px.jpg");
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 402);
            this.Controls.Add(this.layoutControlMainMenu);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "MainMenu";
            this.Opacity = 0.95D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FACM Techno Mark Automatic";
            this.Load += new System.EventHandler(this.MainMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMainMenu)).EndInit();
            this.layoutControlMainMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManagerMainMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlMain)).EndInit();
            this.panelControlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlPanel)).EndInit();
            this.layoutControlPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEditTechno.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControlMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemNavBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel;
        private DevExpress.XtraLayout.LayoutControl layoutControlMainMenu;
        private DevExpress.XtraNavBar.NavBarControl navBarControlMain;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroupAdmin;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroupProgram;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlMain;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemNavBar;
        private DevExpress.XtraEditors.PanelControl panelControlMain;
        private DevExpress.XtraLayout.LayoutControl layoutControlPanel;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupPanel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPanel;
        private DevExpress.XtraEditors.PictureEdit pictureEditTechno;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPicture;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManagerMainMenu;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.Utils.ImageCollection imageCollection;
        private DevExpress.XtraBars.BarStaticItem barStaticItemVersion;
        private DevExpress.XtraEditors.LabelControl labelControlFor;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.RadioGroup radioGroup;
        private DevExpress.XtraNavBar.NavBarItem navBarItemParameters;
        private DevExpress.XtraNavBar.NavBarItem navBarItemPassword;
        private DevExpress.XtraNavBar.NavBarItem navBarItemMain;
        private DevExpress.XtraNavBar.NavBarItem navBarItemExit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private DevExpress.XtraEditors.LabelControl labelControlVersion;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemVersion;
        private DevExpress.XtraNavBar.NavBarItem navBarItemHistory;

    }
}