﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;

namespace FACMTechnoAutomatic
{
    public partial class History : DevExpress.XtraEditors.XtraForm
    {
        private MainMenu parent;
        public History(MainMenu parent)
        {
            this.parent = parent;
            InitializeComponent();
        }

        private void History_Load(object sender, EventArgs e)
        {
            textEditValue.Enabled = false;
            dateEditFrom.DateTime = DateTime.Now;
            dateEditTo.DateTime = DateTime.Now;

            if (!parent.IsFeatureOn(MainMenu.Feature.EXPORT_HISTORY))
            {
                this.simpleButtonExport.Enabled = false;
            }

            this.gridViewParameters.ShowingEditor += new CancelEventHandler(gridViewParameters_ShowingEditor);
        }

        void gridViewParameters_ShowingEditor(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
        }

        private void radioGroupMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radioGroupMode.SelectedIndex == 0)
            {
                textEditValue.Enabled = false;
                dateEditFrom.Enabled = true;
                dateEditTo.Enabled = true;
                textEditPattern.Enabled = true;
            }
            else
            {
                textEditPattern.Enabled = false;
                textEditValue.Enabled = true;
                dateEditFrom.Enabled = false;
                dateEditTo.Enabled = false;
            }
            gridControl.DataSource = null;
        }

        private void simpleButtonSearch_Click(object sender, EventArgs e)
        {
            LoadTable();
        }

        private void LoadTable()
        {
            string query = "";

            if (radioGroupMode.SelectedIndex == 0)
            {
                query += "Select * from (";
                query += "Select * from FACM_History where date >= '" +
                    EditDate(dateEditFrom.DateTime) + "' and date <= '" +
                    EditDate(dateEditTo.DateTime) + "'";
                query += ") as x where x.Sequence like '";   
            }
            else
            {
                query += "Select * from FACM_History where Sequence like '";
            }

            if (radioGroupPattern.SelectedIndex == 1 || radioGroupPattern.SelectedIndex == 2)
            {
                query += "%";
            }
            query += radioGroupMode.SelectedIndex == 0 ? textEditPattern.Text : textEditValue.Text;
            if (radioGroupPattern.SelectedIndex == 0 || radioGroupPattern.SelectedIndex == 1)
            {
                query += "%";
            }
            query += "' order by printed desc";

            DataTable datView = MainMenu.dataLayer.SelectQuery(query);

            DataTable dat = new DataTable();
            int rowCount = datView.Rows.Count;

            dat.Columns.Add("Order", typeof(string));
            foreach (DataColumn col in datView.Columns)
                dat.Columns.Add(col.Caption, typeof(string));
            

            for (int i = 0; i < rowCount; i++)
            {
                object[] param = new object[dat.Columns.Count];
                param[0] = i + 1;
                param[1] = ShowDate((DateTime)datView.Rows[i][0]);
                param[2] = datView.Rows[i][1].ToString();
                param[3] = ShowDateTime((DateTime)datView.Rows[i][2]);
                dat.Rows.Add(param);
            }

            this.gridControl.DataSource = dat;
        }

        #region Date Function
        internal string EditDate(DateTime input)
        {
            int tanggal = input.Day;
            int bulan = input.Month;
            int tahun = input.Year;
            string date = (tanggal < 10 ? "0" : "") + tanggal.ToString();
            string month = (bulan < 10 ? "0" : "") + bulan.ToString();
            string year = tahun.ToString();
            return year + "/" + month + "/" + date;
        }

        internal string ShowDateTime(DateTime input)
        {
            int tanggal = input.Day;
            int bulan = input.Month;
            int tahun = input.Year;
            int detik = input.Second;
            int menit = input.Minute;
            int jam = input.Hour;
            string date = (tanggal < 10 ? "0" : "") + tanggal.ToString();
            string month = (bulan < 10 ? "0" : "") + bulan.ToString();
            string year = tahun.ToString();
            string second = (detik < 10 ? "0" : "") + detik.ToString();
            string minute = (menit < 10 ? "0" : "") + menit.ToString();
            string hour = (jam < 10 ? "0" : "") + jam.ToString();
            return date + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + second;
        }

        internal string ShowDate(DateTime input)
        {
            int tanggal = input.Day;
            int bulan = input.Month;
            int tahun = input.Year;
            string date = (tanggal < 10 ? "0" : "") + tanggal.ToString();
            string month = (bulan < 10 ? "0" : "") + bulan.ToString();
            string year = tahun.ToString();
            return date + "/" + month + "/" + year;
        }

        private string ValidateDate(string inp)
        {
            if (inp.Length == 1)
                return "0" + inp;
            else
                return inp;
        }

        private string FormatDate(string date)
        {
            string[] dateTime = date.Split(" ".ToCharArray());
            string[] time = dateTime[0].Split("/".ToCharArray());
            return time[2] + "/" + time[1] + "/" + time[0] + " " + dateTime[1];
        }
        #endregion

        private void dateEditFrom_EditValueChanged(object sender, EventArgs e)
        {
            if (dateEditFrom.EditValue == null)
            {
                dateEditFrom.DateTime = DateTime.Now;
            }
            
        }

        private void dateEditTo_EditValueChanged(object sender, EventArgs e)
        {
            if (dateEditTo.EditValue == null)
            {
                dateEditTo.DateTime = DateTime.Now;
            }
        }

        private void radioGroupPattern_SelectedIndexChanged(object sender, EventArgs e)
        {
            gridControl.DataSource = null;
        }

        private void simpleButtonDelete_Click(object sender, EventArgs e)
        {
            bool err = false;
            HashSet<int> set = new HashSet<int>();
            string[] token = textEditDelete.Text.Split(",".ToCharArray());
            foreach (string k in token)
            {
                if (k == "")
                {
                    err = true;
                    break;
                }

                int n = int.Parse(k);
                if (n > gridViewParameters.DataRowCount || n == 0) {
                    err = true;
                    break;
                }
                if (!set.Contains(n-1))
                {
                    set.Add(n-1);
                }
            }

            if (!err)
            {
                if (MessageBox.Show("Are you sure want to delete those items?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foreach (int row in set)
                    {
                        string sequence = gridViewParameters.GetRowCellDisplayText(row, gridViewParameters.Columns["Sequence"]);
                        string printed = gridViewParameters.GetRowCellDisplayText(row, gridViewParameters.Columns["Printed"]);

                        MainMenu.dataLayer.ExecuteQuery("delete from FACM_History where sequence = '" +
                            sequence + "' and printed = '" + FormatDate(printed) + "'"
                        );
                    }
                    LoadTable();
                }
                else
                {
                    return;
                }
            }
            else
            {
                MessageBox.Show("Delete Sequence is not valid!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            textEditDelete.Text = "";
        }

        private void simpleButtonExport_Click(object sender, EventArgs e)
        {
            if (gridViewParameters.DataRowCount != 0)
            {
                folderBrowserDialog.ShowDialog();
                string path = @folderBrowserDialog.SelectedPath;
                if (path != "")
                {
                    string report = "report-" + DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + ".txt";
                    path = (path + "\\" + report);
                    string[] lines = new string[gridViewParameters.DataRowCount];

                    for (int i = 0; i < gridViewParameters.DataRowCount; i++)
                    {
                        lines[i] = FormatReport(gridViewParameters.GetRowCellDisplayText(i, gridViewParameters.Columns["Date"]),
                            gridViewParameters.GetRowCellDisplayText(i, gridViewParameters.Columns["Sequence"]),
                            gridViewParameters.GetRowCellDisplayText(i, gridViewParameters.Columns["Printed"])
                        );
                    }

                    System.IO.File.WriteAllLines(path, lines);
                }
            }
            else
            {
                MessageBox.Show("Please Search some value", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private string FormatReport(string date, string sequence, string printed)
        {
            int d1 = date.Length;
            int d2 = sequence.Length;
            StringBuilder build = new StringBuilder();
            build.Append(" ");
            build.Append(date);
            build.Append(generateSpace(15 - d1));
            build.Append(" ");
            build.Append(sequence);
            build.Append(generateSpace(25 - d2));
            build.Append(" ");
            build.Append(printed);
            return build.ToString();
        }

        private string generateSpace(int n)
        {
            StringBuilder build = new StringBuilder();
            for (int i = 0; i < n; i++)
            {
                build.Append(' ');
            }
            return build.ToString();
        }
    }
}