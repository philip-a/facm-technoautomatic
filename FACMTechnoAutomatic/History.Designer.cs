﻿namespace FACMTechnoAutomatic
{
    partial class History
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.gridViewParameters = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemTextEditX = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.textEditDelete = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonDelete = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButtonSearch = new DevExpress.XtraEditors.SimpleButton();
            this.radioGroupPattern = new DevExpress.XtraEditors.RadioGroup();
            this.textEditValue = new DevExpress.XtraEditors.TextEdit();
            this.radioGroupMode = new DevExpress.XtraEditors.RadioGroup();
            this.dateEditTo = new DevExpress.XtraEditors.DateEdit();
            this.dateEditFrom = new DevExpress.XtraEditors.DateEdit();
            this.textEditPattern = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonExport = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemMode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDateFrom = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemDateTo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemPattern = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDelete.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupPattern.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPattern.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPattern)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.gridControl);
            this.layoutControl1.Controls.Add(this.textEditDelete);
            this.layoutControl1.Controls.Add(this.simpleButtonDelete);
            this.layoutControl1.Controls.Add(this.simpleButtonSearch);
            this.layoutControl1.Controls.Add(this.radioGroupPattern);
            this.layoutControl1.Controls.Add(this.textEditValue);
            this.layoutControl1.Controls.Add(this.radioGroupMode);
            this.layoutControl1.Controls.Add(this.dateEditTo);
            this.layoutControl1.Controls.Add(this.dateEditFrom);
            this.layoutControl1.Controls.Add(this.textEditPattern);
            this.layoutControl1.Controls.Add(this.simpleButtonExport);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(720, 246, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(613, 418);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // gridControl
            // 
            this.gridControl.Location = new System.Drawing.Point(12, 121);
            this.gridControl.MainView = this.gridViewParameters;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditX});
            this.gridControl.Size = new System.Drawing.Size(589, 285);
            this.gridControl.TabIndex = 5;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewParameters});
            // 
            // gridViewParameters
            // 
            this.gridViewParameters.GridControl = this.gridControl;
            this.gridViewParameters.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridViewParameters.Name = "gridViewParameters";
            this.gridViewParameters.OptionsView.ShowGroupPanel = false;
            this.gridViewParameters.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            // 
            // repositoryItemTextEditX
            // 
            this.repositoryItemTextEditX.AutoHeight = false;
            this.repositoryItemTextEditX.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditX.Name = "repositoryItemTextEditX";
            // 
            // textEditDelete
            // 
            this.textEditDelete.Location = new System.Drawing.Point(96, 65);
            this.textEditDelete.Name = "textEditDelete";
            this.textEditDelete.Properties.Mask.EditMask = "[0-9,]*";
            this.textEditDelete.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.textEditDelete.Size = new System.Drawing.Size(174, 20);
            this.textEditDelete.StyleController = this.layoutControl1;
            this.textEditDelete.TabIndex = 14;
            // 
            // simpleButtonDelete
            // 
            this.simpleButtonDelete.Location = new System.Drawing.Point(201, 89);
            this.simpleButtonDelete.Name = "simpleButtonDelete";
            this.simpleButtonDelete.Size = new System.Drawing.Size(69, 22);
            this.simpleButtonDelete.StyleController = this.layoutControl1;
            this.simpleButtonDelete.TabIndex = 13;
            this.simpleButtonDelete.Text = "Delete";
            this.simpleButtonDelete.Click += new System.EventHandler(this.simpleButtonDelete_Click);
            // 
            // simpleButtonSearch
            // 
            this.simpleButtonSearch.Location = new System.Drawing.Point(12, 89);
            this.simpleButtonSearch.Name = "simpleButtonSearch";
            this.simpleButtonSearch.Size = new System.Drawing.Size(107, 22);
            this.simpleButtonSearch.StyleController = this.layoutControl1;
            this.simpleButtonSearch.TabIndex = 12;
            this.simpleButtonSearch.Text = "Search";
            this.simpleButtonSearch.Click += new System.EventHandler(this.simpleButtonSearch_Click);
            // 
            // radioGroupPattern
            // 
            this.radioGroupPattern.Location = new System.Drawing.Point(373, 12);
            this.radioGroupPattern.Name = "radioGroupPattern";
            this.radioGroupPattern.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Begin"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Middle"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "End")});
            this.radioGroupPattern.Size = new System.Drawing.Size(228, 27);
            this.radioGroupPattern.StyleController = this.layoutControl1;
            this.radioGroupPattern.TabIndex = 11;
            this.radioGroupPattern.SelectedIndexChanged += new System.EventHandler(this.radioGroupPattern_SelectedIndexChanged);
            // 
            // textEditValue
            // 
            this.textEditValue.Location = new System.Drawing.Point(96, 41);
            this.textEditValue.Name = "textEditValue";
            this.textEditValue.Size = new System.Drawing.Size(174, 20);
            this.textEditValue.StyleController = this.layoutControl1;
            this.textEditValue.TabIndex = 8;
            // 
            // radioGroupMode
            // 
            this.radioGroupMode.Location = new System.Drawing.Point(96, 12);
            this.radioGroupMode.Name = "radioGroupMode";
            this.radioGroupMode.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Date"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Value")});
            this.radioGroupMode.Size = new System.Drawing.Size(174, 25);
            this.radioGroupMode.StyleController = this.layoutControl1;
            this.radioGroupMode.TabIndex = 7;
            this.radioGroupMode.SelectedIndexChanged += new System.EventHandler(this.radioGroupMode_SelectedIndexChanged);
            // 
            // dateEditTo
            // 
            this.dateEditTo.EditValue = null;
            this.dateEditTo.Location = new System.Drawing.Point(373, 91);
            this.dateEditTo.Name = "dateEditTo";
            this.dateEditTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditTo.Size = new System.Drawing.Size(228, 20);
            this.dateEditTo.StyleController = this.layoutControl1;
            this.dateEditTo.TabIndex = 6;
            this.dateEditTo.EditValueChanged += new System.EventHandler(this.dateEditTo_EditValueChanged);
            // 
            // dateEditFrom
            // 
            this.dateEditFrom.EditValue = null;
            this.dateEditFrom.Location = new System.Drawing.Point(373, 67);
            this.dateEditFrom.Name = "dateEditFrom";
            this.dateEditFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFrom.Size = new System.Drawing.Size(228, 20);
            this.dateEditFrom.StyleController = this.layoutControl1;
            this.dateEditFrom.TabIndex = 5;
            this.dateEditFrom.EditValueChanged += new System.EventHandler(this.dateEditFrom_EditValueChanged);
            // 
            // textEditPattern
            // 
            this.textEditPattern.Location = new System.Drawing.Point(373, 43);
            this.textEditPattern.Name = "textEditPattern";
            this.textEditPattern.Size = new System.Drawing.Size(228, 20);
            this.textEditPattern.StyleController = this.layoutControl1;
            this.textEditPattern.TabIndex = 10;
            // 
            // simpleButtonExport
            // 
            this.simpleButtonExport.Location = new System.Drawing.Point(123, 89);
            this.simpleButtonExport.Name = "simpleButtonExport";
            this.simpleButtonExport.Size = new System.Drawing.Size(74, 22);
            this.simpleButtonExport.StyleController = this.layoutControl1;
            this.simpleButtonExport.TabIndex = 15;
            this.simpleButtonExport.Text = "Export";
            this.simpleButtonExport.Click += new System.EventHandler(this.simpleButtonExport_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemMode,
            this.layoutControlItemValue,
            this.layoutControlItem,
            this.layoutControlItemDateFrom,
            this.layoutControlItemDateTo,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItemPattern,
            this.emptySpaceItem2,
            this.layoutControlItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(613, 418);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItemMode
            // 
            this.layoutControlItemMode.Control = this.radioGroupMode;
            this.layoutControlItemMode.CustomizationFormText = "Mode";
            this.layoutControlItemMode.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemMode.Name = "layoutControlItemMode";
            this.layoutControlItemMode.Size = new System.Drawing.Size(262, 29);
            this.layoutControlItemMode.Text = "Mode";
            this.layoutControlItemMode.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItemValue
            // 
            this.layoutControlItemValue.Control = this.textEditValue;
            this.layoutControlItemValue.CustomizationFormText = "Value";
            this.layoutControlItemValue.Location = new System.Drawing.Point(0, 29);
            this.layoutControlItemValue.Name = "layoutControlItemValue";
            this.layoutControlItemValue.Size = new System.Drawing.Size(262, 24);
            this.layoutControlItemValue.Text = "Value";
            this.layoutControlItemValue.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem
            // 
            this.layoutControlItem.Control = this.radioGroupPattern;
            this.layoutControlItem.CustomizationFormText = "Pattern Mode";
            this.layoutControlItem.Location = new System.Drawing.Point(277, 0);
            this.layoutControlItem.Name = "layoutControlItem";
            this.layoutControlItem.Size = new System.Drawing.Size(316, 31);
            this.layoutControlItem.Text = "Pattern Mode";
            this.layoutControlItem.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItemDateFrom
            // 
            this.layoutControlItemDateFrom.Control = this.dateEditFrom;
            this.layoutControlItemDateFrom.CustomizationFormText = "Date From :";
            this.layoutControlItemDateFrom.Location = new System.Drawing.Point(277, 55);
            this.layoutControlItemDateFrom.Name = "layoutControlItemDateFrom";
            this.layoutControlItemDateFrom.Size = new System.Drawing.Size(316, 24);
            this.layoutControlItemDateFrom.Text = "Date From";
            this.layoutControlItemDateFrom.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItemDateTo
            // 
            this.layoutControlItemDateTo.Control = this.dateEditTo;
            this.layoutControlItemDateTo.CustomizationFormText = "Date To     :";
            this.layoutControlItemDateTo.Location = new System.Drawing.Point(277, 79);
            this.layoutControlItemDateTo.Name = "layoutControlItemDateTo";
            this.layoutControlItemDateTo.Size = new System.Drawing.Size(316, 24);
            this.layoutControlItemDateTo.Text = "Date To    ";
            this.layoutControlItemDateTo.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButtonSearch;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 77);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(111, 26);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButtonDelete;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(189, 77);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(73, 26);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEditDelete;
            this.layoutControlItem4.CustomizationFormText = "Delete Sequence";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 53);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(262, 24);
            this.layoutControlItem4.Text = "Delete Sequence";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridControl;
            this.layoutControlItem5.CustomizationFormText = " ";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 103);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(593, 295);
            this.layoutControlItem5.Text = " ";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(81, 1);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItemPattern
            // 
            this.layoutControlItemPattern.Control = this.textEditPattern;
            this.layoutControlItemPattern.CustomizationFormText = "Pattern";
            this.layoutControlItemPattern.Location = new System.Drawing.Point(277, 31);
            this.layoutControlItemPattern.Name = "layoutControlItemPattern";
            this.layoutControlItemPattern.Size = new System.Drawing.Size(316, 24);
            this.layoutControlItemPattern.Text = "Pattern";
            this.layoutControlItemPattern.TextSize = new System.Drawing.Size(81, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(262, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(15, 103);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButtonExport;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(111, 77);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(78, 26);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // History
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 418);
            this.Controls.Add(this.layoutControl1);
            this.Name = "History";
            this.Text = "History";
            this.Load += new System.EventHandler(this.History_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDelete.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupPattern.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPattern.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemPattern)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.DateEdit dateEditTo;
        private DevExpress.XtraEditors.DateEdit dateEditFrom;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDateFrom;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemDateTo;
        private DevExpress.XtraEditors.RadioGroup radioGroupMode;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemMode;
        private DevExpress.XtraEditors.TextEdit textEditPattern;
        private DevExpress.XtraEditors.TextEdit textEditValue;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemValue;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemPattern;
        private DevExpress.XtraEditors.RadioGroup radioGroupPattern;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem;
        private DevExpress.XtraEditors.TextEdit textEditDelete;
        private DevExpress.XtraEditors.SimpleButton simpleButtonDelete;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSearch;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewParameters;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditX;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.SimpleButton simpleButtonExport;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
    }
}